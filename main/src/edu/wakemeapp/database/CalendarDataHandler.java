/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter,
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 *
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Handles creation and upgrading of the CalendarEvent database.
 * @author Viktor Sj�lind, Erik Forsman
 *
 */
public final class CalendarDataHandler extends SQLiteOpenHelper {

	private static CalendarDataHandler instance = null;
	public static final String TABLE_EVENTS = "events";
	public static final String EVENTS_COLUMN_ID = "_id";
	public static final String EVENTS_COLUMN_EVENT_ID = "event_id";
	public static final String EVENTS_COLUMN_EVENT_NAME = "event_name";
	public static final String EVENTS_COLUMN_STARTTIME = "start_time";
	public static final String EVENTS_COLUMN_LONGITUDE = "longitude";
	public static final String EVENTS_COLUMN_LATITUDE = "latitude";
	private static final String DATABASE_TEST_NAME = "test_events.db";
	private static final String DATABASE_NAME = "events.db";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_CREATE = "create table "
		      + TABLE_EVENTS + "("
		      + EVENTS_COLUMN_ID + " integer primary key autoincrement, "
		      + EVENTS_COLUMN_EVENT_ID + " text not null unique, "
		      + EVENTS_COLUMN_EVENT_NAME + " text not null, "
		      + EVENTS_COLUMN_LATITUDE + " real not null, "
		      + EVENTS_COLUMN_LONGITUDE + " real not null"
		      + ");";


	/**
	 * @param context Active android context.
	 * @param test The boolean test is used to activate testmode when testing the database.
	 */
	private CalendarDataHandler(final Context context, final boolean test) {
		super(context, getDatabaseName(test), null, DATABASE_VERSION);
	}
	/**
	 * Singleton.
	 * @param context Active context
	 * @param test The boolean test is used to activate testmode when testing
	 * the database.
	 * @return returns an instance of this class.
	 */
	public static synchronized CalendarDataHandler getInstance(
			final Context context, final boolean test) {
		if (instance == null) {
			instance = new CalendarDataHandler(context, test);
		}
		return instance;
	}

	@Override
	public void onCreate(final SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(final SQLiteDatabase database, final int oldVersion,
			final int newVersion) {
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
	    onCreate(database);
	}

	/**
	 * Toggle between test and normal.
	 * @param test The boolean test is used to activate testmode when testing the database.
	 * @return Active database.
	 */
	public static String getDatabaseName(final boolean test) {
		if(test) {
			return DATABASE_TEST_NAME;
		} else {
			return DATABASE_NAME;
		}
	}
}
