/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter,
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 *
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.database;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import edu.wakemeapp.CalendarEvent;

/**
 * Enables reading and writing to and from the CalendarEvent database.
 * @author Viktor Sjölind, Erik Gil Forsman
 *
 */
public final class CalendarDataSource {

	private static CalendarDataSource instance = null;
	private SQLiteDatabase database;
	private CalendarDataHandler dbHandler;
	private String[] allColumns = { CalendarDataHandler.EVENTS_COLUMN_ID,
			CalendarDataHandler.EVENTS_COLUMN_EVENT_ID,
			CalendarDataHandler.EVENTS_COLUMN_EVENT_NAME,
			CalendarDataHandler.EVENTS_COLUMN_LATITUDE,
			CalendarDataHandler.EVENTS_COLUMN_LONGITUDE};

	/**
	 * Singleton.
	 * @param context Active android context.
	 * @param test The boolean test is used to activate testmode when testing
	 * the database.
	 */
	private CalendarDataSource(final Context context, final boolean test) {
		if (context == null) {
			throw new IllegalArgumentException("Context cannot be null");
		}
		dbHandler = CalendarDataHandler.getInstance(context, test);
	}

	/**
	 * Singleton.
	 * @param context Active context.
	 * @param test	Indicates if testmode is on/off.
	 * @return	Returns an instance of this class
	 */
	public static synchronized CalendarDataSource getInstance(
			final Context context, final boolean test) {
		if (instance == null) {
			instance = new CalendarDataSource(context, test);
		}
		return instance;
	}

	/**
	 * Opens the a connection to the database.
	 * @throws SQLException If database does not exist.
	 */
	public void open() throws SQLException {
		database = dbHandler.getWritableDatabase();
	}

	/**
	 * Closes the connection to the database.
	 */
	public void close() {
		dbHandler.close();
	}

	/**
	 * Adds an event to the database.
	 * @param event The Event to add to the database.
	 * @return the id in the database of the added event as a long. -1 if
	 * Event already exists.
	 */
	public long addEvent(final CalendarEvent event) {
		return addEvent(event.getId(), event.getEventName(), event.getLatitude()
				, event.getLongitude());
	}

	/**
	 * The Event to put in to the database as parameters.
	 * @param id The Event id as a String.
	 * @param name The name of the event as a String
	 * @param latitude the latitude as a double.
	 * @param longitude the longitude as a double.
	 * @return the id in the database of the added event as a long. -1 if Event
	 * already exists.
	 */
	public long addEvent(final String id, final String name,
			final double latitude, final double longitude) {
		ContentValues values = new ContentValues();
		values.put(CalendarDataHandler.EVENTS_COLUMN_EVENT_ID, id);
		values.put(CalendarDataHandler.EVENTS_COLUMN_EVENT_NAME, name);
		values.put(CalendarDataHandler.EVENTS_COLUMN_LATITUDE, latitude);
		values.put(CalendarDataHandler.EVENTS_COLUMN_LONGITUDE, longitude);

		try {
			Log.d(this.getClass().getName(), "Trying to add event with id '" + id
					+ ", " + values.get(CalendarDataHandler.
							EVENTS_COLUMN_LATITUDE) + ", " + values.get(
									CalendarDataHandler.EVENTS_COLUMN_LONGITUDE)
									+ "' to database.");

			return database.insert(CalendarDataHandler.TABLE_EVENTS, null, values);
		} catch (SQLiteConstraintException e) {
			Log.d(this.getClass().getName(), "Event with id '" + id + "' "
					+ "already exists in database.");
			return -1;
		}
	}

	/**
	 * Translates a {@link Cursor} to a {@link CalendarEvent}.
	 * @param cursor The {@link Cursor} to translate, cannot be null.
	 * @return The result of the translation as a {@link CalendarEvent}.
	 * @throws IllegalArgumentException Thrown if cursor is null.
	 */
	private CalendarEvent cursorToEvent(final Cursor cursor) throws IllegalArgumentException {
		if (cursor == null) {
			throw new IllegalArgumentException("Cursor cannot be null!");
		}
		if (cursor.isAfterLast()) {
			throw new IllegalArgumentException("Cursor is not pointing "
					+ "at anything");
		}

		CalendarEvent event = new CalendarEvent();

		event.setEventID(cursor.getString(1));
		event.setEventName(cursor.getString(2));
		event.setLatitude(cursor.getDouble(3));
		event.setLongitude(cursor.getDouble(4));

		return event;
	}

	/**
	 * Deletes an {@link CalendarEvent} from the database.
	 * @param event The {@link CalendarEvent} to delete from the database.
	 * @return True if the deletion was a success, else false.
	 */
	public boolean deleteEvent(final CalendarEvent event) {
		if (event == null) {
			throw new IllegalArgumentException("deleteEvent: event cannot be null");
		}
		return deleteEvent(event.getId());
	}


	/**
	 * Deletes an event from the database.
	 * @param id Id of the event that is supposed to be deleted.
	 * @return True if 1 or more rows are deleted.
	 */
	public boolean deleteEvent(final String id) {
		if (id == null) {
			throw new IllegalArgumentException("deleteEvent: id cannot be null");
		}
		int deletedRows = database.delete(CalendarDataHandler.TABLE_EVENTS,
				CalendarDataHandler.EVENTS_COLUMN_EVENT_ID + "=\"" + id + "\"", null);
		Log.d(this.getClass().getName(), deletedRows + " rows deleted.");
		return deletedRows > 0;
	}

	/**
	 * Clears the database removing EVERY {@link CalendarEvent} entry.
	 */
	public void clear() {
		Set<CalendarEvent> set = getAllEvents();
		for (CalendarEvent x: set) {
			deleteEvent(x);
		}
	}

	/**
	 * @return Returns a {@link Set} containing all {@link CalendarEvent}
	 * instances in the database.
	 */
	public Set<CalendarEvent> getAllEvents() {
		Set<CalendarEvent> events = new HashSet<CalendarEvent>();

		Cursor cursor = getCursorFromQuery(null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			CalendarEvent event = cursorToEvent(cursor);
			events.add(event);
			cursor.moveToNext();
		}

		cursor.close();
		return events;
	}

	/**
	 * Checks for an {@link CalendarEvent} with the given id from the database.
	 * If found the {@link CalendarEvent} is returned, else
	 * {@link NoSuchElementException} will be thrown.
	 * @param id The id to check for.
	 * @return The Entity matching the provided id.
	 * @throws NoSuchElementException Thrown if there is no entry in the
	 * database with the provided id.
	 */
	public CalendarEvent getEvent(final String id) throws NoSuchElementException {
		if (id == null) {
			throw new IllegalArgumentException("getEvent: id cannot be null");
		}

		Cursor cursor = getCursorFromQuery(CalendarDataHandler.
				EVENTS_COLUMN_EVENT_ID + "=\"" + id + "\"");
		cursor.moveToFirst();
		if (cursor.isAfterLast()) {
			throw new NoSuchElementException("No Element with the id '" + id
					+ "'" + " found in the database");
		}
		return cursorToEvent(cursor);
	}

	/**
	 * Checks if a given {@link CalendarEvent} exists in the database.
	 * @param event The {@link CalendarEvent} to check for.
	 * @return True if the {@link CalendarEvent} exists, else false.
	 */
	public boolean containsEvent(final CalendarEvent event) {
		if (event == null) {
			throw new IllegalArgumentException("containsEvent: event "
					+ "cannot be null");
		}
		return containsEvent(event.getId());
	}

	/**
	 * Checks if an {@link CalendarEvent} with the given id exists in the database.
	 * @param id The id to check for.
	 * @return True if the {@link CalendarEvent} exists, else false.
	 */
	public boolean containsEvent(final String id) {
		if (id == null) {
			throw new IllegalArgumentException("containsEvent: id cannot "
					+ "be null");
		}

		Cursor cursor = getCursorFromQuery(CalendarDataHandler.
				EVENTS_COLUMN_EVENT_ID + "=\"" + id + "\"");
		return cursor.moveToFirst();
	}

	/**
	 * Returns an cursor made by a query in the events database.
	 * The selection String must be formated as an SQL WHERE where the WHERE
	 * part is excluded.
	 * If the selection String is null ALL rows will be selected.
	 * @param selection The selection expression describing which rows to
	 * select, if null ALL rows will be selected.
	 * @return The cursor pointing to the first instance in the database.
	 */
	private Cursor getCursorFromQuery(final String selection) {
		return database.query(CalendarDataHandler.TABLE_EVENTS, allColumns,
				selection, null, null, null, null, null);
	}

	/**
	 * Prints all events in LogCat. Used for debugging.
	 */
	public void printAll() {
		for (CalendarEvent e : getAllEvents()) {
			Log.d(this.getClass().getSimpleName(), "EventId:\t" + e.getEventID() + "\n\tEventName:\t" + e.getEventName() + "\n\tLat:\t" + e.getLatitude() + "\n\tLong:\t" +e.getLongitude());
		}
	}
}
