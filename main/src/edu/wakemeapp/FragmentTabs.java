/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import java.util.Set;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import edu.wakemeapp.settings.SettingsActivity;

/**
 * Responsible for tab handling (creating, set tablisteners, onTabSelected etc..).
 * @author kristofferskjutar
 *
 */
public class FragmentTabs extends Activity{
	private String mAccountName;
	private String mOwnerName;
	protected static boolean toggleOnBackPressed = false;
	private static final int NUMBER_OF_DAYS_TO_PARSE = 7;

	/**
	 * Initialize the bars and set up one listener each.
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final ActionBar bar = getActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		Log.d("wakeup", "FragmentTabs onCreate");
		bar.addTab(bar.newTab()
				.setText("wakeup")
				.setTabListener(new TabListener<WakeUpFragment>(
						this, "wakeup", WakeUpFragment.class)));
		bar.addTab(bar.newTab()
				.setText("travel")
				.setTabListener(new TabListener<TravelFragment>(
						this, "travel", TravelFragment.class)));


		if (savedInstanceState != null) {
			bar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
		}
		Set<String> tmpSet = PreferenceManager.getDefaultSharedPreferences(this).getStringSet(getString(R.string.keyParsedEvents), null);
		if (tmpSet != null) {
			parseCal();
		}
	}
	
	/**
	 * Save the selected tab.
	 */
	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
	}


	/**
	 * Set up the menu.
	 */
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		getActionBar().setIcon(R.drawable.wakemeapp_white);
		return true;
	}

	/**
	 * Get email address.
	 */
	private void getEmailAddress() {

		Account[] accounts = AccountManager.get(this).getAccounts();
		for (Account account : accounts) {
			if (Patterns.EMAIL_ADDRESS.matcher(account.name).matches()) {
				mAccountName = account.name;
				mOwnerName = account.name;
				Log.d("test", mAccountName);
			}
		}
	}



	/**
	 * An example of how to start the parser, see the b.put* especially.
	 */
	private void calendarParserTest() {
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		CalendarParseFragment calendarFragment = new CalendarParseFragment();
		Bundle b = new Bundle();
		b.putString(getString(R.string.keyAccountName), mAccountName);
		b.putString(getString(R.string.keyAccountType), "com.google");
		b.putString(getString(R.string.keyOwnerName), mOwnerName);
		b.putInt(getString(R.string.keyDaysAhead), NUMBER_OF_DAYS_TO_PARSE);
		calendarFragment.setArguments(b);
		fragmentTransaction.add(calendarFragment, getString(R.string.tagCalendarParseFragment));
		fragmentTransaction.commit();
	}




	/**
	 * A listener that handles navigation between tabs.
	 * @author kristofferskjutar
	 * @param <T>
	 */
	public static class TabListener<T extends Fragment> implements ActionBar.TabListener {
		private final Activity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		private final Bundle mArgs;
		private Fragment mFragment;

		public TabListener(final Activity activity, final String tag, final Class<T> clz) {
			this(activity, tag, clz, null);
		}

		/**
		 * Set up the tablistener to a given fragment.
		 * @param activity the call comes from
		 * @param tag name of tab
		 * @param clz class of fragment
		 * @param args 
		 */
		public TabListener(Activity activity, String tag, Class<T> clz, Bundle args) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
			mArgs = args;
			Log.d("tabListener", "new Tablistener ");
			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state.  If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			mFragment = mActivity.getFragmentManager().findFragmentByTag(mTag);
			if (mFragment != null && !mFragment.isDetached()) {
				FragmentTransaction ft = mActivity.getFragmentManager().beginTransaction();
				ft.hide(mFragment);
				ft.commit();
			}
		}

		/**
		 * Called when this specific tab is selected and displays.
		 * it
		 * @param tab selected
		 * @param ft fragment it handled
		 */
		public final void onTabSelected(final Tab tab, final FragmentTransaction ft) {
			if (mFragment == null) {
				mFragment = Fragment.instantiate(mActivity, mClass.getName(), mArgs);
				ft.add(android.R.id.content, mFragment, mTag);
			} else {
				ft.show(mFragment);
			}
		}

		/**
		 * Called when this specific tab is unselected and hides it.
		 * @param tab unselected tab
		 * @param ft fragment from
		 */
		public final void onTabUnselected(final Tab tab, final FragmentTransaction ft) {
			if (mFragment != null) {
				ft.hide(mFragment);
			}
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// TODO Auto-generated method stub
			
		}
	}

	/**
	 * Called when an item in the menu is selected and takes appropriate.
	 * actions
	 */
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {

		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this,SettingsActivity.class);
			startActivity(settingsIntent);
			return true;

		case R.id.parsebutton:
			parseCal();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Runned when user clicks on parse button.
	 */
	private void parseCal() {
		getEmailAddress();
		calendarParserTest();
	}

	/**
	 * Launches the parsing of calendar.
	 */
	public final void parse(final View v) {
		parseCal();
	}

	@Override
	public void onBackPressed() {
		toggleOnBackPressed = true;
		this.finish();
	}
}
