/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

/**
 * GPSTracker is used to provide current location.
 * @author Erik Forsman
 */

public class GPSTracker extends Service implements LocationListener {

	private Context mContext;
    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean isPossible = false;
    private LocationManager locationManager;
    private Location location = null;
    private double latitude = 0;
    private double longitude = 0;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    /**
     * Starts the getLocation proccess.
     * @param context Active context.
     */
    public GPSTracker(final Context context) {
        this.mContext = context;
        getLocation();
        stopUsingGPS();
    }

    /**
     * Uses either GPS or Network to provide current position.
     * @return Location object with the current position
     */
    private Location getLocation() {
		try{

			locationManager = (LocationManager) mContext
	                .getSystemService(LOCATION_SERVICE);

	        isGPSEnabled = locationManager
	                .isProviderEnabled(LocationManager.GPS_PROVIDER);

	        isNetworkEnabled = locationManager
	                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

	        if (!isGPSEnabled && !isNetworkEnabled) {
	        	Log.d("GPS", "GPS and Network offline");
	        } else {
	        	Log.d("GPS", "GPS or Network enabled");
	        	isPossible = true;
	        	if (isNetworkEnabled) {

	            	Log.d("GPS", "Network enabled");
	                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	                if (locationManager != null) {
	                    location = locationManager.getLastKnownLocation(
	                    		LocationManager.NETWORK_PROVIDER);
	                    if (location != null) {
	                        latitude = location.getLatitude();
	                        Log.d("GPS", "" + location.getLatitude());
	                        longitude = location.getLongitude();
	                    }
	                }
	        	}

	        	if (isGPSEnabled) {
	            	Log.d("GPS", "GPS Enabled");
	                if (location == null) {

	                    locationManager.requestLocationUpdates(LocationManager.
	                    		GPS_PROVIDER, MIN_TIME_BW_UPDATES,
	                    		MIN_DISTANCE_CHANGE_FOR_UPDATES , this);


	                    if (locationManager != null) {
	                        location = locationManager
	                                .getLastKnownLocation(LocationManager.
	                                		GPS_PROVIDER);
	                        Log.d("GPS", "" + location.getLatitude());
	                        if (location != null) {
	                        	latitude = 0;
	                        	longitude = 0;
	                            latitude = location.getLatitude();
	                            longitude = location.getLongitude();
	                        }
	                    }
	                }
	            }
	        }
		} catch (Exception e) {
            e.printStackTrace();
        }
        return location;
	}
	/**
	 * @return Latitude of current position.
	 */
	public final double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }
        return latitude;
    }

	/**
	 * @return Longitude of current position.
	 */
	public final double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }
	/**
	 * Removes all location updates.
	 */
	public final void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

	@Override
	public void onLocationChanged(final Location location) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderDisabled(final String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProviderEnabled(final String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(final String provider, final int status, final Bundle extras) {
		// TODO Auto-generated method stub
	}

	@Override
	public IBinder onBind(final Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return True if its possible to retrieve location.
	 */
    public final boolean canGetLocation() {
        return this.isPossible;
    }

    /**
     * Creates a warning window.
     * Called if GPS AND network are off.
     */
    public final void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setTitle("GPS settings");
        alertDialog.setMessage("GPS is not enabled. "
        + "Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
                dialog.cancel();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int which) {
            dialog.cancel();
            }
        });
        alertDialog.show();
    }
}
