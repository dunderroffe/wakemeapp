/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Geocode which takes a town, street or address(as String) and delegates the
 * lat, long coordinates for the given place to whoever calls it.
 * @author Martin Helmersson
 *
 */
public class Geocode extends AsyncTask<String, Void, String> {
	public GeoResponse delegate = null;
	private static final int STATUS_CODE_OK = 200;

	/**
	 * Default constructor.
	 */
	public Geocode() {
	}

	/**
	 * Private method called by doInBackground.
	 * Gets the String that is return from the request.
	 * @param url the URL for the get request.
	 * @return string, the returned value from request
	 */
	private String readJson(final String url) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == STATUS_CODE_OK) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.d("Geocode", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	/**
	 * Called when doInBackground is finished.
	 * Takes a String and turns it into a JSON and parses the values
	 * Also delegates the result to the caller
	 * @param result of the parsing
	 */
	@Override
	protected void onPostExecute(final String result) {
		result.trim();
		String address = "";
		double lng = -1.0;
		double lat = -1.0;
		try {
			JSONObject jsonObject = new JSONObject(result);
			//Checks if the JSON contains valid information
			if (jsonObject.getString("status").equals("OK")) {
				address = ((JSONArray)jsonObject.get("results"))
						.getJSONObject(0).getString("formatted_address");
				lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
						.getJSONObject("geometry").getJSONObject("location")
						.getDouble("lng");

				lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
						.getJSONObject("geometry").getJSONObject("location")
						.getDouble("lat");
				delegate.processFinish(lat,lng, address, true);
			} else {
				delegate.processFinish(lat,lng, address, false);
			}
		} catch (JSONException e) {
			Log.d("Geocode", "failed to generate JSON");
			e.printStackTrace();
			delegate.processFinish(lat,lng, address, false);
		}
	}

	@Override
	/**
	 * Building the link for the request
	 * @param the street, town or place user is looking for
	 * @return a string with the info from the request
	 */
	protected String doInBackground(final String... url) {
		url[0] = url[0].trim();
		Log.d("cbl", "Before regex:" + url[0]);
		String temp = url[0].replaceAll("\\W+", "+");
		String address = temp.replaceAll("\\s+", "");
		Log.d("cbl", "After regex:" + address);

		StringBuilder build = new StringBuilder();
		build.append("http://maps.googleapis.com/maps/api/geocode/json?address=");
		build.append(address);
		build.append("&sensor=false");
		Log.d("cbl", build.toString());
		return readJson(build.toString());


	}

}

