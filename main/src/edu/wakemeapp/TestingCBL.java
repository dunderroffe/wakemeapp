/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj���lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import android.content.Context;
import android.util.Log;
import edu.wakemeapp.database.CalendarDataSource;

/**
 * This class is used to test CorrectBadLocation.
 * AsyncTask does not work in JUnit
 * @author Erik Forsman
 *
 */
public class TestingCBL {

	private String newAddress = "lindholmen+g�teborg";
	private CalendarEvent event;
	private String dbId = "hem";
	private CorrectBadLocation cbl;
	private CalendarDataSource mDb;
	private final boolean test = true; //true for test mode
	private static final double LAT = 57.333; // Magic numbers
	private static final double LNG = 12.456;

	/**
	 * Simple test method.
	 * @param mContext Active context
	 */

	public TestingCBL(final Context mContext) {

		Log.d("cbl", "Setting up test  enivironment");
		event = new CalendarEvent(LAT, LNG, 0, 0, dbId);
		mDb = CalendarDataSource.getInstance(mContext, test);
		mDb.open();
		Log.d("cbl", "Printing OLD lat: " + LAT);
		Log.d("cbl", "Printing OLD long: " + LNG);
		cbl = new CorrectBadLocation(event, mContext, newAddress);
		Log.d("cbl", "Finishing setup");
		mDb.close();

	}
}
