/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * carrier object, with informatin regrding the arrival place for travel.
 * @author Kevin
 */
public class Towards {
	private String displayType;
	private String type;
	private String text;
    /**
     * json of mot.
     * @param jsonObject object Mot
     */
	public Towards(final JSONObject jsonObject) throws JSONException {
			displayType = jsonObject.getString("@displaytype");
			type = jsonObject.getString("@type");
			text = jsonObject.getString("#text");
		}
	/**
	 * getter for the travel type.
	 * @return displayType
	 */
	public final String getDisplayType() {
		return displayType;
	}
	/**
	 * getter for the travel type.
	 * @return type
	 */
	public final String getType() {
		return type;
	}
	/**
	 * getter for the text.
	 * @return text
	 */
	public final String getText() {
		return text;
	}
}