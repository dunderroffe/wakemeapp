/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * depatrue object, with informatin regrding the departiure for the travel.
 * @author Kevin
 */
public class Departure {
	private Location location;
	private String dateTime;
	/**
	 * json of departure.
	 * @param jsonObject Object for departure
	 */
	public Departure(final JSONObject jsonObject) {
		try {
			dateTime = jsonObject.getString("datetime");
			location = new Location(jsonObject.getJSONObject("location"));
		} catch (JSONException e) {
		}
	}
	/**
	 * getter for a location object.
	 * @return location
	 */
	public final Location getlocation() {
		return location;
	}
	/**
	 * getter for a string with the date and time.
	 * @return dateTime
	 */
	public final String getDateTime(){
		return dateTime;
	}
}