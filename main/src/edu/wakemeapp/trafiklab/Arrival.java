/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Arrival object, with informatin regrding travel arrival.
 * @author Kevin
 */
public class Arrival {
	private Location location;
	private String dateTime;

    /**
     * json of arrival.
     * @param jsonObject object containing arrival info
     */
	public Arrival(final JSONObject jsonObject) throws JSONException{
		try {
			dateTime = jsonObject.getString("datetime");
		} catch (JSONException e) {
		}
		location = new Location(jsonObject.getJSONObject("location"));
	}
	/**
	 * getter for a location object.
	 * @return location
	 */
	public final Location getlocation() {
		return location;
	}
	/**
	 * getter for a string with the date and time.
	 * @return datetimes
	 */
	public final String getDateTime() {
		return dateTime;
	}
}
