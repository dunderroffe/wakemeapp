/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * station object, with informatin regrding the station.
 * @author Kevin
 */
public class Station {
	private String stationName;
	private int stationId;
    /**
     * json of station
     * @param jsonObject
     */
	public Station(final JSONObject jsonObject) {
		try {
			stationName = jsonObject.getString("name");
			stationId = jsonObject.getInt("@id");
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	public Station(int id, String string) {
		stationId = id;
		stationName = string;
	}
	public int getStationId(){
		return stationId;
	}
	public String getStationName(){
		return stationName;
	}
}
