/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * carrier object, with informatin regrding the carrier for travel.
 * @author Kevin
 */
public class Carrier {
	private String carrierName;
	private String carrierUrl;
	private int carrierId;
	private String carrierNumber;

    /**
     * json of carrier.
     * @param json.
     */
	public Carrier(JSONObject jsonObject) throws JSONException {
		carrierName = jsonObject.getString("name");
		carrierUrl = jsonObject.getString("url");
		carrierId = jsonObject.getInt("id");
		carrierNumber = jsonObject.getString("number");
	}
	/**
	 * getter for a string with the carrier name.
	 * @return carrierName
	 */
	public final String getCarrierName() {
		return carrierName;
	}
	/**
	 * getter for a string with the carrier url.
	 * @return carrierUrl
	 */
	public final String getCarrierUrl() {
		return carrierUrl;
	}
	/**
	 * getter for the carrier id.
	 * @return carrierId
	 */
	public final int getCarrierId() {
		return carrierId;
	}
	/**
	 * getter for the carrier number.
	 * @return carrierNumber
	 */
	public final String getCarrierNumber() {
		return carrierNumber;
	}

}