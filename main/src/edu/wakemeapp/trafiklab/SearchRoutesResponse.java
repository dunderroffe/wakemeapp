/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import java.util.ArrayList;

/**
 * response of time table items.
 * @author Kevin
 */

public interface SearchRoutesResponse {
	void processFinishSearch(ArrayList<Ttitem> ttitems); 
}
