/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * creates list of time table objects from a json.
 * @author Kevin
 */


public class Ttitem {
	private List<List<RouteSegment>> segment;
	/**
	 * Constructor.
	 * @param jobject JsonArray for ttitem.
	 * @throws JSONException
	 */
	public Ttitem(final JSONArray jobject) throws JSONException {
		for (int i = 0; i < jobject.length(); i++) {
			try {
			addARouteSegmen(new RouteSegment(jobject.getJSONObject(i)));
			} catch (JSONException e)	{
				JSONArray array = jobject.getJSONArray(i);
				ArrayList<RouteSegment> buff = new ArrayList<RouteSegment>();
				for (int j = 0; j < array.length(); j++) {
					buff.add(new RouteSegment(jobject.getJSONObject(i)));
				}
				addArrayRouteSegmen(buff);
			}
		}
	}
	/**
	 * Object-constructor for ttitem.
	 * @param jsonObject jsonObject
	 * @throws JSONException
	 */
	public Ttitem(final JSONObject jsonObject) throws JSONException {
		try {
		JSONArray segments = jsonObject.getJSONArray("segment");
		ArrayList<RouteSegment> buff = new ArrayList<RouteSegment>();
		for (int i = 0; i < segments.length(); i++) {
			buff.add(new RouteSegment(segments.getJSONObject(i)));
		}
		addArrayRouteSegmen(buff);
		} catch (JSONException e) {
			JSONObject segment = jsonObject.getJSONObject("segment");
			addARouteSegmen(new RouteSegment(segment));
		}
	}
	/**
	 * Adds an array to a segment.
	 * @param segment
	 * @return
	 */
	private final Boolean addArrayRouteSegmen(final List<RouteSegment> segment) {
		if (this.segment == null) {
			this.segment = new ArrayList<List<RouteSegment>>();
		}
		return this.segment.add(segment);
	}
	/**
	 * Adds route segment.
	 * @param segment segment to add.
	 * @return true if success
	 */
	private final Boolean addARouteSegmen(final RouteSegment segment){
		if(this.segment == null)
			this.segment = new ArrayList<List<RouteSegment>>();

		List<RouteSegment> buff = new ArrayList<RouteSegment>();
		buff.add(segment);
		return this.segment.add(buff);
	}
	/**
	 * returns a list with the route segments.
	 * @param routesegment
	 */
	public final List<List<RouteSegment>> getRouteSegmen() {
		return this.segment;
	}

}
