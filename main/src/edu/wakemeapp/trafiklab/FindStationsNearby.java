/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj���lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;


/**
 * FindStationsNearby a url string to get a json.
 * @author kevin
 *
 */
public class FindStationsNearby {
	private Station station;

	private String apiKey = "52988ab428aa3a3a16204c18b685c956";
	private String apiVersion = "2.1";
	private String urlTrafikLab = "https://api.trafiklab.se/samtrafiken/resrobot/";

	public FindStationsNearby(final double longitude, final double latitude, final int radius){

		String urlStations = (urlTrafikLab + "StationsInZone.json?apiVersion="
				+ apiVersion + "&centerX=" 	+ longitude + "&centerY="
				+ latitude + "&radius=" + radius + "&coordSys=WGS84&key=" + apiKey);

		StationsNearby task = new StationsNearby();
		task.execute(new String[] {urlStations});
		task.delegate = null;
	}
}
