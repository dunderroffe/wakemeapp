/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;

	/**
	 * An object contaning the bas of a route segement.
	 * @author Kevin
	 *
	 */
	public class RouteSegment {
		private SegmentTime segmentTid;
		private Departure departure;
		private Arrival arrival;
		private String direction;
	    /**
	     * json of route.
	     * @param jsonObject
	     */
	public RouteSegment(final JSONObject jsonObject) throws JSONException {
		segmentTid = new SegmentTime(jsonObject.getJSONObject("segmentid"));

		try {
		direction = jsonObject.getString("direction");
		} catch (Exception e) {
			e.printStackTrace();
		}
		departure = new Departure(jsonObject.getJSONObject("departure"));
		arrival = new Arrival(jsonObject.getJSONObject("arrival"));

		}
	public final SegmentTime getSegmentTid() {
		return this.segmentTid;
	}
	public final String getDirection() {
		return direction;
	}
	public final Departure getDeparture() {
		return this.departure;
	}
	public final Arrival getArrival() {
		return this.arrival;
	}
}