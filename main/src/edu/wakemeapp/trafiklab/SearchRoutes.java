/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

/**
 * SearchRoutes a url string  and delegates the travel response for the given place to whoever calls it.
 * @author kevin & martin
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class SearchRoutes extends AsyncTask<String, Void, String> {
	public SearchRoutesResponse delegate = null;
	private static final int STATUS_CODE_OK = 200;

	@Override
	protected String doInBackground(final String... url) {
		Log.d("Trafik", "Url: " + url[0]);
		return readJson(url[0]);
	}
	/**
	 * Private method called by doInBackground.
	 * Gets the String that is return from the request
	 * which later is turn in to a JSON
	 * @param url the URL for the get request
	 * @return string, the returned value from request
	 */
	private String readJson(final String url) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == STATUS_CODE_OK) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.d("FAIL", "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

	@Override
	/**
	 * Called when doInBackground is finished.
	 * Takes a String and turns it into a JSON and parses the values
	 * Also delegates the result to the caller
	 */
	protected void onPostExecute(final String result) {
		result.trim();
		ArrayList<Ttitem> ttitems = null;
		try {
			JSONObject jsonObject = new JSONObject(result);
			if (jsonObject.getString("timetableresult").contains("ttitem")) {
				jsonObject.getJSONObject("timetableresult");
				JSONArray jsonArray = jsonObject.getJSONObject("timetableresult").getJSONArray("ttitem");
				ttitems = new ArrayList<Ttitem>();

				if (jsonArray != null) {
					for (int i = 0; i < jsonArray.length(); i++) {
						Ttitem ttitem = new Ttitem(jsonArray.getJSONObject(i));
						ttitems.add(ttitem);
					}
				} else {
					JSONObject object = jsonObject.getJSONObject("ttitem");
					Ttitem ttitem = new Ttitem(object);
					ttitems.add(ttitem);
				}
			}
		} catch (JSONException e) {
			Log.d("FAIL", "failed to generate JSON");
			e.printStackTrace();
		}
		delegate.processFinishSearch(ttitems);
	}
}