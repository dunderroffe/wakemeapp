/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Contains location for arrival and departure, contains stations name, id, and its cordinates.
 * @author Kevin
 *
 */
public class Location {
	private int stationId;
	private double longitude;
	private double latitude;
	private String name;
    /**
     * json of location.
     * @param jsonObject object for location
     */
	public Location(JSONObject jsonObject) throws JSONException {
			stationId = jsonObject.getInt("@id");
			longitude = jsonObject.getDouble("@x");
			latitude = jsonObject.getDouble("@y");
			name = jsonObject.getString("name");
	}
	/**
	 * getter for a int with the stations id.
	 * @return stationId
	 */
	public final int getStationId() {
		return stationId;
	}
	/**
	 * getter for stations longitude.
	 * @return longitude
	 */
	public final double getLongitude() {
		return longitude;
	}
	/**
	 * getter for stations longitude.
	 * @return latitude
	 */
	public final double getLatitude() {
		return latitude;
	}
	/**
	 * getter for stations name.
	 * @return name
	 */
	public final String getName() {
		return name;
	}
}