/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.trafiklab;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author Kevin
 *
 */
public class SegmentTime {
	private Towards mot;
	private Carrier carrier;
	/**
	 * 
	 * @param jsonObject
	 * @throws JSONException
	 */
	public SegmentTime(final JSONObject jsonObject) throws JSONException {
		try {
			mot = new Towards(jsonObject.getJSONObject("mot"));
			carrier = new Carrier(jsonObject.getJSONObject("carrier"));
		} catch (JSONException e) {
		}
	}
	public Towards getMot(){
		return mot;
	}
	public Carrier getCarrier(){
		return carrier;
	}

}
