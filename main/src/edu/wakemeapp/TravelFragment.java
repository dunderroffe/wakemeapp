/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter,
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.w3c.dom.Document;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import edu.wakemeapp.database.CalendarDataSource;
import edu.wakemeapp.googlemaps.DirectionsResponse;
import edu.wakemeapp.googlemaps.GMapV2Direction;
import edu.wakemeapp.googlemaps.GetDirectionsDoc;
import edu.wakemeapp.trafiklab.Carrier;
import edu.wakemeapp.trafiklab.SearchRoutes;
import edu.wakemeapp.trafiklab.SearchRoutesResponse;
import edu.wakemeapp.trafiklab.SegmentTime;
import edu.wakemeapp.trafiklab.Station;
import edu.wakemeapp.trafiklab.StationsNearby;
import edu.wakemeapp.trafiklab.StationsNearbyResponse;
import edu.wakemeapp.trafiklab.Ttitem;

/**
 * Draws a google map in the traveltab which contains travel information to.
 * the user and handles all travels
 * Needs google play services
 * @author Martin Helmersson
 *
 */
public class TravelFragment extends Fragment implements DirectionsResponse,SearchRoutesResponse, StationsNearbyResponse{

	private Context mContext;
	private static final String HOME_LOCATION_VARIABLE = "home";
	private BroadcastReceiver receiver = new BroadcastReceiver() {



		@Override
		public void onReceive(final Context context, final Intent intent) {
			mContext = context;
			String action = intent.getAction();
			Log.d("TravelFragment", "Action: " + action);
			if (action.equals(context.getString(R.string.actionDestinationSet))) {
				Log.d("TravelFragment", "Destination is set, starting map fragment");

				sPreference = PreferenceManager.getDefaultSharedPreferences(context);
				travelMode = sPreference.getString(KEY_MEANS_OF_TRAVEL, null);
				CalendarEvent destinationEvent = null;
				String eventString = sPreference.getString(context.getString(R.string.earliestEvent), null);
				if (eventString != null) {
					destinationEvent = new CalendarEvent(eventString);
				}
				Log.d("Trafik", "Travelmode:" + travelMode);

				dbSource = CalendarDataSource.getInstance(context, test);
				dbSource.open();
				boolean test = false;
				boolean apa = test;

				if (destinationEvent == null) {
					textview.setText("No destination was found"
				+ "\nLoad calendar or no events in calendar");
				} else {
					apa = true;
					long startTimeNextEvent = destinationEvent.getEventStartTime();
					travelDate = new SimpleDateFormat("yyyy-MM-dd").
							format(startTimeNextEvent);
					travelTime = new SimpleDateFormat("HH:mm").
							format(startTimeNextEvent);
					Log.d("Trafik", "next event:" + travelDate + ", " + travelTime);
					destination = new LatLng(destinationEvent.getLatitude(),destinationEvent.getLongitude());
					map.addMarker(new MarkerOptions().position(destination).
							title("Destination"));
				}
				if (!dbSource.containsEvent(HOME_LOCATION_VARIABLE)) {
					textview.setText("Home location is not set!"
							+ "\nGo to settings and verify your home location");
				} else {
					test = true;
					home = new CalendarEvent();
					setHomePos();
					myPosition = new LatLng(home.getLatitude(), home.getLongitude());
					map.addMarker(new MarkerOptions().position(myPosition).title("Home"));
				}
				if (apa && test) {
					if (travelMode.equals("Walk")) {
						getDirections("walking");
					} else if (travelMode.equals("Car")) {
						getDirections("driving");
					} else if (travelMode.equals("Public Transport")) {
						findStationsNearby(myPosition.longitude, myPosition.latitude,
								SEARCH_RADIUS, DEPATURE);
					}
				}
				dbSource.close();
			}
		}
	};



	private GoogleMap map;
	private LatLng myPosition = null;
	private LatLng destination = null;
	private GMapV2Direction md;
	private CalendarDataSource dbSource;
	private boolean test = false;
	private TextView textview;
	private View v;

	private int depStationId;
	private int arrStationId;

	private String apiKey = "52988ab428aa3a3a16204c18b685c956";
	private String apiVersion = "2.1";
	private String urlTrafikLab = "https://api.trafiklab.se/samtrafiken/resrobotsuper/";

	private static final int SEARCH_RADIUS = 5000;
	private static final String ARRIVAL = "arrival";
	private static final String DEPATURE = "depature";
	private String travelTime;
	private String travelDate;

	private String dateString;
	private String[] parts;
	private long firstDate, secondDate;
	private SharedPreferences sPreference;
	private static final String TEMP_LOCATION_DATES = "tempLocationDates";
	private static final String TEMP_LOCATION_KEY = "tempLocationKey";
	private static final String KEY_MEANS_OF_TRAVEL = "means of travel";
	private static final int TRAVEL_MODE_ARRAY_SIZE = 5;
	private Calendar time;
	private CalendarEvent home;
	private String travelMode;

	/**
	 * Creates the map and the textview shown in the tab.
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d("TravelFragment", "onCreateView started");
		v = inflater.inflate(R.layout.travel_layout, container, false);

		md = new GMapV2Direction();
		map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
		if (myPosition == null) {
			// if null set the map to middle of sweden, required
			map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(59.299552, 14.916), 5));
		} else {
			map.animateCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 11));
		}
		textview = (TextView) v.findViewById(R.id.resInfo);
		return v;
	}

	@Override
	public void onResume() {
		map.clear();
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		Set<String> events = sharedPref.getStringSet(getString(R.string.keyParsedEvents), null);
		if (events == null) {
			Toast.makeText(getActivity(), getString(R.string.pleaseLoad), Toast.LENGTH_LONG).show();
		} else {
			IntentFilter filter = new IntentFilter();
			filter.addAction(getString(R.string.actionDestinationSet));
			LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
			Log.d("TravelFragment", "Fragment has been entered, requesting destination to be set.");
			Intent requestDestination = new Intent();
			requestDestination.setAction(getActivity().getString(R.string.actionRequestDestination));
			getActivity().sendBroadcast(requestDestination);
			Log.d("TravelFragment", "BroadCast sent with action: " + requestDestination.getAction() + ", awaiting response.....");
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	/**
	 * Uses google directions API for travel search between two points.
	 * Starts a new getDirectionsDoc task
	 * @param travelMode search travel by driving or walking
	 */
	public void getDirections(final String travelMode) {
		Log.d("TravelFragment", "MyLat: " + myPosition.toString() + "DestLat: "
				+ destination.toString());
		String[] list = new String[TRAVEL_MODE_ARRAY_SIZE];
		list[0] = String.valueOf(myPosition.latitude);
		list[1] = String.valueOf(myPosition.longitude);
		list[2] = String.valueOf(destination.latitude);
		list[3] = String.valueOf(destination.longitude);
		if (travelMode.equals("driving")) {
			list[4] = GetDirectionsDoc.MODE_DRIVING;
		} else {
			list[4] = GetDirectionsDoc.MODE_WALKING;
		}
		GetDirectionsDoc task = new GetDirectionsDoc();
		task.execute(list);
		task.delegate = this;

	}

	/**
	 * Executes when the directions task i finished and draw out the route.
	 * on the map
	 * @param doc containing the route info
	 */
	@Override
	public void processFinish(final Document doc) {
		if (md.getStatus(doc)) {
			String duration = md.getDurationText(doc);
			String distance = md.getDistanceText(doc);
			Log.d("TIME", "Duration: " + duration + "  Distance: " + distance);
			String startAddress = md.getStartAddress(doc);
			String endAddress = md.getEndAddress(doc);

			ArrayList<LatLng> directionPoint = md.getDirection(doc);
			PolylineOptions rectLine = new PolylineOptions().width(6).color(Color.BLUE);

			for (int i = 0; i < directionPoint.size(); i++) {
				rectLine.add(directionPoint.get(i));
			}

			map.addPolyline(rectLine);
			setZoom(rectLine);

			textview.setText("Travelmode: " + travelMode + "\nFrom: "
					+ startAddress + "\n" + "To: " + endAddress
					+ "\n" + "Distance: " + distance + " Estimated time: "
					+ duration);
		} else {
			textview.setText("Not able to find a suitable route \n"
					+ "Home:" + myPosition.toString() + "\nDestination: " + destination.toString());
			map.addMarker(new MarkerOptions().position(myPosition).title("Home"));
			map.addMarker(new MarkerOptions().position(destination).title("Destination"));

		}
	}

	/**
	 * Creates a new searchRoutes task which later returns the travel info.
	 * by the processFinish method (Public Transport)
	 * @param fromId departure station
	 * @param toId  arrival station
	 * @param date date for arrival or departure
	 * @param time time for arrival or departure
	 * @param arrival if true specify arrival time, ow false
	 * @param highSpeedTrains if true include high speed trains
	 * @param trains if true include trains
	 * @param busses if true include busses
	 * @param boats if true include boats
	 * @param expressBusses if true include express busses
	 */
	public final void searchTrafikLab(final int fromId, final int toId, final String date,
			final String time, final boolean arrival, final boolean highSpeedTrains, final boolean trains,
			final boolean busses, final boolean boats, final boolean expressBusses) {
		String urlSearch = (urlTrafikLab + "/Search.json?apiVersion=" + apiVersion + "&coordSys=WGS84" + "&fromId=" + fromId + "&toId=" + toId
				+ "&date=" + date + "&time=" + time	+ "&arrival=" + arrival + "&searchType=F"
				+ "&mode1=" + highSpeedTrains + "&mode2=" + trains + "&mode3=" + busses + "&mode4=" + boats + "&mode5=" + expressBusses + "&key=" + apiKey);
		SearchRoutes task = new SearchRoutes();
		task.execute(new String[] {urlSearch});
		task.delegate = this;
	}

	/**
	 * This method uses trafiklab to find nearby Stations.
	 * @param longitude  longs for the position where to search stations
	 * @param latitude  lats for the position where to search stations
	 * @param radius search radius how far away too search stations
	 * @param type departure or arrival
	 */
	public final void findStationsNearby(final double longitude, final double latitude, final int radius, final String type) {
		String apiKey = "52988ab428aa3a3a16204c18b685c956";
		String apiVersion = "2.1";
		String urlTrafikLab = "https://api.trafiklab.se/samtrafiken/resrobot/";
		String urlStations = (urlTrafikLab + "StationsInZone.json?apiVersion="
				+ apiVersion + "&centerX=" 	+ longitude + "&centerY="
				+ latitude + "&radius=" + radius + "&coordSys=WGS84&key=" + apiKey);

		StationsNearby task = new StationsNearby();
		String[] arr = new String[2];
		arr[0] = urlStations;
		arr[1] = type;
		task.execute(arr);
		task.delegate = this;
	}

	/**
	 * Executes when findStationsNearby(departure) is finished.
	 * @param stations takes a list of stations found in nearby home
	 */
	@Override
	public final void processFinishDepStation(final ArrayList<Station> stations) {
		depStationId = stations.get(0).getStationId();
		findStationsNearby(destination.longitude, destination.latitude,
				SEARCH_RADIUS, ARRIVAL);
		Log.d("Trafik", "DEP STATION");

	}
	/**
	 * Executes when findStationsNearby(arrival) is finished.
	 * By now both stations id:s is collected and we can now search a travel
	 * @param stations, takes a list of stations found in the nearby area
	 */
	@Override
	public void processFinishArrivalStation(final ArrayList<Station> stations) {
		arrStationId = stations.get(0).getStationId();
		searchTrafikLab(depStationId, arrStationId, travelDate,travelTime,
				true, true, true, true, true, true);
		Log.d("Trafik", "ARR STATION");
	}

	/**
	 * Executes when searchrouts task is done and handles the information.
	 * from public transport travel and also draws it on the map
	 *
	 *@param item a list of items containing travelinfo
	 */
	@Override
	public void processFinishSearch(final ArrayList<Ttitem> item) {
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> depTimes = new ArrayList<String>();
		ArrayList<String> arrTimes = new ArrayList<String>();
		ArrayList<LatLng> latlngs = new ArrayList<LatLng>();
		ArrayList<String> lines = new ArrayList<String>();
		Log.d("Trafik", "TravelFragment received info! Begin Parse");
		Ttitem ttitem = item.get(0);
		for (int i = 0; i < ttitem.getRouteSegmen().size(); i++) {
			for (int j = 0; j < ttitem.getRouteSegmen().get(i).size(); j++) {
                double lat = (ttitem.getRouteSegmen().get(i).get(j).getDeparture().getlocation().getLatitude());
                double lng = (ttitem.getRouteSegmen().get(i).get(j).getDeparture().getlocation().getLongitude());
                double arrlat = (ttitem.getRouteSegmen().get(i).get(j).getArrival().getlocation().getLatitude());
                double arrlng = (ttitem.getRouteSegmen().get(i).get(j).getArrival().getlocation().getLongitude());
                latlngs.add(new LatLng(lat,lng));
                latlngs.add(new LatLng(arrlat,arrlng));
				String depName = (ttitem.getRouteSegmen().get(i).get(j).
						getDeparture().getlocation().getName());
				String arrName = (ttitem.getRouteSegmen().get(i).get(j).
						getArrival().getlocation().getName());
				names.add(depName);
				names.add(arrName);
				String depTime = (ttitem.getRouteSegmen().get(i).get(j).getDeparture().getDateTime());
				depTimes.add(depTime);

				String arrTime = (ttitem.getRouteSegmen().get(i).get(j).getArrival().getDateTime());
				arrTimes.add(arrTime);

				SegmentTime sTid = ttitem.getRouteSegmen().get(i).get(j).getSegmentTid();
				Carrier c = sTid.getCarrier();
				String line = "Walk";
				if (c != null) {
					 line = c.getCarrierNumber();
				}
				lines.add(line);
			}


		}
		PolylineOptions rectLine = new PolylineOptions().width(6).color(Color.BLUE);

		for (int i = 0; i < latlngs.size(); i++) {
			if (i == 0) {
				map.addMarker(new MarkerOptions().position(latlngs.get(i)).title(names.get(i)).snippet("Line: " + lines.get(0) + " at: " + depTimes.get(0)));
				rectLine.add(latlngs.get(i));
			}
			if (i == (latlngs.size() - 1)) {
				map.addMarker(new MarkerOptions().position(latlngs.get(i)).title(names.get(i)).snippet("Arrival Time: " + arrTimes.get(arrTimes.size() - 1)));
				rectLine.add(latlngs.get(i));
			}
			for (int j = 1; j < depTimes.size(); j++) {
				if (i < (latlngs.size() - 1) && i > 0) {
					map.addMarker(new MarkerOptions().position(latlngs.get(i)).title(names.get(i)).snippet("Line: " + lines.get(j) + " at: " + depTimes.get(j)));
					rectLine.add(latlngs.get(i));
				}

			}


		}
		textview.setText("Travelmode: " + travelMode + "\nFrom: " + names.get(0) + "\nTo: " + names.get(names.size() - 1)
				+ "\nDeparture Time: " + depTimes.get(0) +  "\nArrival Time: " + arrTimes.get(arrTimes.size() - 1));
		map.addPolyline(rectLine);
		setZoom(rectLine);
	}

	/**
	 * Sets the homeposition and checks if the temporary homepos is in use.
	 */
	private void setHomePos() {
		sPreference = PreferenceManager.getDefaultSharedPreferences(mContext);
		time = Calendar.getInstance();
		dateString = sPreference.getString(TEMP_LOCATION_KEY, null);
		if (dateString != null && dbSource.containsEvent("tempHome")) {
			parts = dateString.split("-");
			firstDate = Long.parseLong(parts[0]);
			secondDate = Long.parseLong(parts[1]);
			if ((firstDate < time.getTimeInMillis()) && (secondDate > time.getTimeInMillis())) {
				home = dbSource.getEvent("tempHome");
			}
		} else {
			home = dbSource.getEvent("home");
		}
	}
	/**
	 * Adapts the zoom of the map to fit all tags.
	 * @param route list of tags
	 */
	private void setZoom(final PolylineOptions route) {
		List<LatLng> points = route.getPoints(); // route is instance of PolylineOptions
	    LatLngBounds.Builder bc = new LatLngBounds.Builder();

	    for (LatLng item : points) {
	        bc.include(item);
	    }

	    map.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
	}
}