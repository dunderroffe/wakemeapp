/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sjolind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import edu.wakemeapp.alarm.AlarmArrayAdapter;
import edu.wakemeapp.alarm.AlarmTimePicker;
/**
 * Handles the WakeUp tab window with it's list of alarms.
 * @author kristofferskjutar
 *
 */
public class WakeUpFragment extends Fragment implements OnClickListener, OnCheckedChangeListener {

	private AlarmArrayAdapter adapter;
	private ArrayList<Long> alarmList;
	private HashMap<Long ,Boolean> stateList;
	private View view;

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(final Context context, final Intent intent) {
			Log.d("receive", "received new alarm: " + intent.getAction());
			 //received new alarms?
			if (intent.getAction().equals("add")) {
				stateList.clear();
				alarmList.clear();
				long[] times = intent.getLongArrayExtra(context.getString(R.string.keyParsedTimes));
				for (int i = 0; i < times.length; i++) {
					Long time = times[i];
					alarmList.add(time);
					stateList.put(time, true);
					Log.d("times in frag", "" + new Date(times[i]));
				}
				updateData();
			}
			if (intent.getAction().equals("update"))	{
				updateData();

			}
			if (intent.getAction().equals("delete")) {
				Long time = intent.getLongExtra("Time", 1L);
				alarmList.remove(time);
				stateList.remove(time);
				updateData();
			}

		}

	};

	/**
	 * Set up the view and update the list.
	 */
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.wakeup_layout, container, false);
		ListView listView = (ListView) view.findViewById(R.id.alarmListview);
		Log.d("wakeup", "getView running");
		if (adapter != null) {
			listView.setAdapter(adapter);
			Log.d("adding", "adding adapter"); //
		}
		updateData(); //update data to display list (changed screen orientation)
		return view;
	}

	/**
	 * initiate variables.
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		alarmList = new ArrayList<Long>();
		stateList = new HashMap<Long, Boolean>();
		adapter = new AlarmArrayAdapter(getActivity(), (ArrayList<Long>)alarmList.clone(), this);
		this.setRetainInstance(true); //no onCreate on screen Tilt or similar
		Log.d("adding", "adding test");
	}
	/**
	 * Called when user used edit-button on a specific alarm and changed it.
	 * @param old
	 * @param updated
	 */
	public final void updateAlarm(final Long old, final Long updated) {
		if (!alarmList.contains(updated)) {
			Boolean oldState = stateList.get(old);
			Boolean sucess = alarmList.remove(old);
			if (!sucess) {
				Log.d("product", "failed to remove");
			}
			alarmList.add(updated);
			stateList.put(updated, oldState);
			Intent updatedAlarm = new Intent();
			updatedAlarm.setAction(getActivity().getString(R.string.updatedFromGui)); //send it to the alarmHandler
			updatedAlarm.putExtra(getActivity().getString(R.string.old), old);
			updatedAlarm.putExtra(getActivity().getString(R.string.updated), updated);
			LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(updatedAlarm);
			updateData();
		}
	}
	/**
	 * Updated the adapter with the current list of alarms.
	 */
	private void updateData() {
		Iterator<Long> it = alarmList.iterator();
		Long currentTime = Calendar.getInstance().getTimeInMillis();
		while (it.hasNext())	{
			Long time = it.next();
			if (time < currentTime) {
				it.remove();
				stateList.remove(time);
			}
		}
		adapter.clear();

		Collections.sort(alarmList);

		//Test logging
		Log.d("product", "deleted adapter");
		for (Long alarm : alarmList) {
			Log.d("product", "" + alarm);
		}
		Log.d("product", "DONE");

		if (alarmList.size() != 0) {
			it = alarmList.iterator();
			while (it.hasNext()) {
				Long time = it.next();
				adapter.insert(time, alarmList.indexOf(time));
			}

		}

		adapter.notifyDataSetChanged();

	}
	/**
	 * When user clicks on edit, launch the timepicker.
	 */
	@Override
	public final void onClick(final View v) {
		Long oldTime = (Long)v.getTag();
		AlarmTimePicker alarmPicker = new AlarmTimePicker();
		alarmPicker.setOldTime(oldTime);
		alarmPicker.setController(this);
		alarmPicker.show(getFragmentManager(), "timePicker");
	}
	/**
	 * When user clicks on a specific on/off switch, send to alarmAdapter.
	 */
	@Override
	public void onCheckedChanged(final CompoundButton c, final boolean on) {
		Intent intent = new Intent();
		Long time = (Long)c.getTag();
		if (on) {
			intent.setAction(getActivity().getString(R.string.on));
			intent.putExtra(getActivity().getString(R.string.time), time);
			LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
		} else {
			intent.setAction(getActivity().getString(R.string.off));
			intent.putExtra(getActivity().getString(R.string.time), time);
			LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
		}
		stateList.put(time, on);
	}

	/**
	 * When returning from another activity (settings for example), update the list.
	 */
	@Override
	public void onResume() {
		updateData();
		Log.d("wakeup", " onResume");
		IntentFilter filter = new IntentFilter();
		filter.addAction("add");
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
		super.onResume();
	}

	public final HashMap<Long, Boolean> getStateList() {
		return stateList;
	}


}