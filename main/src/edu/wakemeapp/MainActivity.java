/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import java.util.Calendar;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import edu.wakemeapp.alarm.AlarmHandler;

/**
 * MainActivity for.
 * @author Mikael Stolpe
 *
 */
public class MainActivity extends Activity {

	private AlarmHandler alarmHand;
	private Intent fragmentTabs;

	private BroadcastReceiver receiver = new BroadcastReceiver() {

		@Override
		public void onReceive(final Context context, final Intent intent) {
			String action = intent.getAction();
			Log.d("action", action);

			if (action.equals(getString(R.string.actionSendTimes))) {
				long[] times = intent.getLongArrayExtra(getString(R.string.keyParsedTimes));
				Log.d("size", "after" + times.length);

				alarmHand.newAlarm(times);
				Calendar ca = Calendar.getInstance();
				Log.d("alarms", "number of alarms: " +  times.length + "in handler: " + alarmHand.intentList.toString());
				//alarmHand.newAlarm(ca.getTimeInMillis()+5000);
				Intent timesForUi = new Intent("add");
				timesForUi.putExtra(context.getString(R.string.keyParsedTimes), times);
				LocalBroadcastManager.getInstance(context).sendBroadcast(timesForUi);
			}
		}
	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wakeup_layout);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false); //set default settings
		IntentFilter filter = new IntentFilter();
		filter.addAction(getString(R.string.actionSendTimes));
		initMainActivity();
	}

	@Override
	protected final void onResume() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(getString(R.string.actionSendTimes));
		filter.addAction(getString(R.string.actionSetNext));
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
		super.onResume();
		/*
		 * Had problems when closing the application.
		 * Empty activity kept running in the background when finishing FragmentTabs.
		 * Simple solution.
		 */
		if (FragmentTabs.toggleOnBackPressed) {
			FragmentTabs.toggleOnBackPressed = false;
			this.finish();
		}
	}

	/**
	 * Initiates the activity setting the relelvant variables and launches the GUI.
	 */
	private void initMainActivity() {
		Log.d("mainActivity", "starting MainAcitivity ");
		Bundle bundle = new Bundle();
		alarmHand = new AlarmHandler(this, bundle);

		fragmentTabs = new Intent(this, FragmentTabs.class);

		startActivity(fragmentTabs);


	}

	@Override
	public void onBackPressed() {
		this.finish();
	}








}
