/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.alarm;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TimePicker;
import edu.wakemeapp.WakeUpFragment;

/**
 * This dialog is launched when a user wants to edit an alarm in the WAKEUP tab.
 * @author kristofferskjutar
 *
 */
public class AlarmTimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener	 {

	private Long old;
	private WakeUpFragment controller;
	private boolean alreadySent = false;


	/**
	 * Set up inital values to the time.
	 */
	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		// Use the current time as the default values for the picker
		final Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
				true);
	}

	public final void setOldTime(final Long oldTime) {
		this.old = oldTime;

	}
	public final void setController(final WakeUpFragment controller) {
		this.controller = controller;
	}

	/**
	 * called when a user sets a time. Grabs the time set and
	 * notifies its controller (WakeUpFragment)
	 */
	@Override
	public final void onTimeSet(final TimePicker view, final int hourOfDay, final int minute) {
		if (!alreadySent) {
			//due to bug in emulator (onTimeSet called twice)

			alreadySent = true;
			Calendar oldTime = Calendar.getInstance();
			oldTime.setTimeInMillis(old);

			Calendar newTime = Calendar.getInstance();

			newTime.set(oldTime.get(Calendar.YEAR),
					oldTime.get(Calendar.MONTH),
					oldTime.get(Calendar.DAY_OF_MONTH),
					hourOfDay, minute);
			Log.d("product", "Sending from Picker");
			controller.updateAlarm(old, newTime.getTimeInMillis());
		}
	}
}
