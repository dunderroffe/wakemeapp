/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.alarm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import edu.wakemeapp.R;
/**
 * @author kristofferskjutar, Gustav Dahl and Martin Helmersson
 * Used to handle all alarms in this application
 *
 */
public class AlarmHandler extends BroadcastReceiver{

	private Context ctext;
	private int alarmId = 0;
	private Long[] alarmIds;
	private static final int INIT_SIZE = 10;
	public ArrayList<AlarmIntent> intentList = new ArrayList<AlarmIntent>();
	private AlarmManager alarmMgr;

	/**
	 * Handles creation and maintenance of alarms.
	 * @param ctext context of calling activity
	 * @param bundle bundle of data
	 */
	public AlarmHandler(final Context ctext, final Bundle bundle) {
		this.ctext = ctext;

		IntentFilter filter = new IntentFilter();
		filter.addAction(ctext.getString(R.string.on));
		filter.addAction(ctext.getString(R.string.off));
		filter.addAction(ctext.getString(R.string.updatedFromGui));
		LocalBroadcastManager.getInstance(ctext).
		registerReceiver(this, filter);
		alarmIds = new Long[INIT_SIZE];
		alarmId = 0;

		alarmMgr =
				(AlarmManager) ctext.
				getSystemService(Context.ALARM_SERVICE);

	}
	/**
	 * Deletes previous alarms and creates new alarms.
	 * @param timeMillis list of times when the alarms is supposed to start
	 */
	public final void newAlarm(final long[] timeMillis) {
		Log.d("alarms", "list size : " + intentList.size());
		clearAlarms();

		for (int i = 0; i < timeMillis.length; i++) {
			//convert time
			long time = timeMillis[i];
			createAlarm(time);
		}
		Log.d("alarms", "list size : " + intentList.size());
	}
	/**
	 * creates an single alarm.
	 * @param time the time when the alarm is supposed to start
	 */
	private void createAlarm(final long time) {

		Calendar c = Calendar.getInstance();
		if(time > c.getTimeInMillis()) {
			long millisToAlarm = (time - c.getTimeInMillis());
			c.setTimeInMillis(millisToAlarm);

			millisToAlarm = c.getTimeInMillis();
			//create PendingIntent
			PendingIntent sender = createPendingIntent(time);

			//get seconds to alarm
			int secondsToAlarm = (int) (millisToAlarm / 1000);
			c.setTimeInMillis(System.currentTimeMillis());
			c.add(Calendar.SECOND, secondsToAlarm);
			//remove seconds
			c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
					c.get(Calendar.DAY_OF_MONTH),
					c.get(Calendar.HOUR_OF_DAY),
					c.get(Calendar.MINUTE), 0);

			Log.d("alarms", "secondsToAlarm : " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND));
			alarmMgr.set(AlarmManager.RTC_WAKEUP,
					c.getTimeInMillis(), sender);
			intentList.add(new AlarmIntent(sender, time));

		}
	}

	/**
	 * Creates PendingIntens for each alarm.
	 * @param time the alarm time when the alarm is supposed to go
	 * @return the pendingIntent
	 */
	private PendingIntent createPendingIntent(final long time) {
		Intent intent = new Intent(ctext, MyAlarm.class);
		intent.setAction("alarm");

		intent.putExtra("time", time);
		return  PendingIntent.getBroadcast(ctext,
				alarmId++, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
	}
	/**
	 * Removes all previous alarms.
	 */
	private void clearAlarms() {

		Log.d("alarms", "list size : " + intentList.size());

		Iterator<AlarmIntent> it = intentList.iterator();
		while (it.hasNext()) {
			Log.d("alarms", "going through!");
			AlarmIntent i = it.next();
			if (i.getIntent() != null) {
				alarmMgr.cancel(i.getIntent());
				i.setIntent(null);
			}
			it.remove();
		}

	}
	/**
	 * Turns of an alarm.
	 * @param id the alarm id
	 */
	public final void alarmOff(final long id) {
		for (int i = 0; i  < intentList.size(); i++) {
			if (intentList.get(i).getId() == id) {
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(intentList.get(i).getId());
				Log.d("alarms", "setting alarm off: " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));

				alarmMgr.cancel(intentList.get(i).getIntent());
				intentList.get(i).setIntent(null);
				break;
			}
		}
	}
	/**
	 * Turns on an alarm.
	 * @param time the alarm time
	 */
	public final void alarmOn(final long time) {
		for (int i = 0; i < intentList.size(); i++) {
			if (intentList.get(i).getId() == time) {
				Log.d("alarms", "setting alarm on with time : " + time);
				PendingIntent intent = createPendingIntent(time);
				//set time
				Calendar c = Calendar.getInstance();
				long millisToAlarm =
						(time - c.getTimeInMillis());
				int secondsToAlarm = (int) (millisToAlarm / 1000);
				c.setTimeInMillis(System.currentTimeMillis());
				c.add(Calendar.SECOND, secondsToAlarm);

				alarmMgr.set(AlarmManager.RTC_WAKEUP,
						c.getTimeInMillis(), intent);

				intentList.get(i).setIntent(intent);

			}
		}
	}
	/**
	 * Removes an specific alarm.
	 * @param i The Intent of the alarm
	 */
	private void clearAlarm(final AlarmIntent i) {
		if (i.getIntent() != null) {
			alarmMgr.cancel(i.getIntent());
			i.setIntent(null);
		}
		intentList.remove(i);
		Log.d("alarms", "removing! ");
	}
	/**
	 * Changes the start time of an alarm to a new alarm.
	 * @param old The old time
	 * @param updated The new time
	 */
	private void editAlarm(final long old, final long updated) {
		for (int i = 0; i < intentList.size(); i++) {
			if (intentList.get(i).getId() == old) {
				Boolean on = true;
				Log.d("alarms", "old time: " + intentList.get(i).getId() + "");
				if (intentList.get(i).getIntent() == null) {
					Log.d("onOff", "is off");
					on = false;
				}
				clearAlarm(intentList.get(i));

				createAlarm(updated);
				if (!on) {
					alarmOff(updated);
				}
			}
		}
	}

	/**
	 * Receives broadcast which is supposed to affect the alarms.
	 */
	@Override
	public void onReceive(final Context context, final Intent intent) {
		Log.d("alarms", "onReceive :" + intent.getAction());
		if (intent.getAction().equals(context.getString(
				R.string.updatedFromGui))) {
			Log.d("onReceive", "updatedFromGui");
			long old = intent.getLongExtra(
					context.getString(R.string.old), 0);
			long updated = intent.getLongExtra(
					context.getString(
							R.string.updated), 0);
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(updated);
			Log.d("alarms", "time from GUI: " + c.get(Calendar.SECOND));
			editAlarm(old, updated);

		} else if (intent.getAction().equals(
				context.getString(R.string.on))) {
			Log.d("onReceive", "on!");
			long id = intent.getLongExtra(context.getString(
					R.string.time), 0);
			alarmOn(id);

		} else if (intent.getAction().equals(
				context.getString(R.string.off))) {
			Log.d("onReceive", "off!");
			long id = intent.getLongExtra(
					context.getString(R.string.time), 0);
			Log.d("alarms", "time off : " + id);
			alarmOff(id);

		} else if (intent.getAction().equals("alarm")) {
			Intent alarmIntent = new Intent(
					ctext, AlarmActivity.class);
			alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			Boolean vibration = intent.getExtras().
					getBoolean("vibration", true);
			alarmIntent.putExtra("VibrationStatus", vibration);
			ctext.startActivity(alarmIntent);

		} else if (intent.getAction().equals(("snooze"))) {
			long time = intent.getLongExtra(
					ctext.getString(R.string.time), 0);
			Log.d("alarms", "creating snooze with time : " + time);
			createAlarm(time);
		}
	}

}