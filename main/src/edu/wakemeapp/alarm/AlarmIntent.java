
/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */
package edu.wakemeapp.alarm;

import android.app.PendingIntent;

/**
 * Sets an pendingIntent for a specific alarm.
 * @author Kristoffer Skjutar
 *
 */
public class AlarmIntent {

	private PendingIntent pendingIntent;
	private long id;
	public Boolean vibration;

	/**
	 * @param intent The PendingIntent which will be paired
	 *  with the specific alarm
	 * @param id The alarm id
	 */
	public AlarmIntent(final PendingIntent intent, final long id) {
		this.pendingIntent = intent;
		this.id = id;
		this.vibration = true;
	}
	
	public final PendingIntent getIntent() {
		return pendingIntent;
	}
	
	public final void setIntent(final PendingIntent intent) {
		this.pendingIntent = intent;
	}

	public final long getId() {
		return id;
	}
	public final void setId(final long id) {
		this.id = id;
	}
}