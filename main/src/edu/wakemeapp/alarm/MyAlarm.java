/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * An alarm which will go off in an specific time.

 * @author Gustav Dahl and Martin Helmersson
 * @version 1.0
 *
 */
public class MyAlarm extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, final Intent intent) {
		Log.d("alarms", "received : " + intent.getAction());
		if (intent.getAction().equals("alarm")) {
			Intent alarmIntent = new Intent(
					context, AlarmActivity.class);
			alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			Boolean vibration = intent.getExtras().
					getBoolean("vibration", true);
			alarmIntent.putExtra("VibrationStatus", vibration);
			context.startActivity(alarmIntent);
		}

	}
}