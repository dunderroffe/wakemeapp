/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.alarm;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import edu.wakemeapp.R;

/**
 * Starts and turns off alarms.
 *
 * @author Gustav
 *
 */
public class AlarmActivity extends Activity implements MediaPlayer.OnPreparedListener {
	private Calendar time = null;
	private static final int SNOOZE_TIME_MILLIS = 5000;
	private Vibrator v;
	private MediaPlayer mMediaPlayer = null;
	private Uri uri;
	private String alarms, currentTime;

	/**
	 * initiates and starts music,vibration and AlertDialog with cancel and
	 * snooze button.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_alarm);
		Log.d("apa", "ALARMACTIVITY TEXT");
		Boolean vibrationToggle = getIntent().getExtras().
				getBoolean("VibrationStatus");
		time = Calendar.getInstance();
		currentTime = new SimpleDateFormat("MMM dd, HH:mm").
				format(time.getTime());

		// Sound
		SharedPreferences getAlarms =
				PreferenceManager.
				getDefaultSharedPreferences(getBaseContext());
		alarms = getAlarms.getString("Ringtone", "defaultRingtone");

		uri = Uri.parse(alarms);
		Log.d("ALARMURI", "" + uri);
		playSound(getApplicationContext(), uri);

		 // Vibration
		 if (vibrationToggle) {
			  v = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);
			 long pattern[] = {0, 800, 200, 1200, 300, 2000, 400, 4000};
			 v.vibrate(pattern, -1);
		 }
		 	//Builds the dialog
			AlertDialog.Builder builder =
					new AlertDialog.Builder(this);
			Log.d("AlarmActivity", currentTime + "Time to wake up!");

			builder.setMessage(currentTime + " Time to wake up!").
			setCancelable(
	            false).setPositiveButton(R.string.cancel,
	            new DialogInterface.OnClickListener() {
	                public void onClick(final DialogInterface dialog, final int id) {
	                    dialog.cancel();
	                    mMediaPlayer.stop();
	                    v.cancel();
	                    finishActivity();
	                    dialog.dismiss();
	                }
	            }).setNegativeButton(R.string.snooze,
	            new DialogInterface.OnClickListener() {
	                public void onClick(final DialogInterface dialog, final int id) {
	                   	dialog.cancel();
	                   	mMediaPlayer.stop();
	                    v.cancel();
	                    dialog.dismiss();
	                	snoozeAlarm();
	             }
	            });
	    	AlertDialog alert = builder.create();
	    	alert.show();
	}

	/**
	 * Inflates the menu. Adds items to the action bar if it is present
	 */
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		getMenuInflater().inflate(R.menu.alarm, menu);
		return true;
	}
	/**
	 * Clears the alarm.
	 */
	public final void clearAlarm() {
		Intent clearedAlarm = new Intent();
		clearedAlarm.setAction("update"); //send it to the WakeUpFragment
		LocalBroadcastManager.getInstance(this).sendBroadcast(clearedAlarm);
	}
	/**
	 * Creates and new alarm a specific amount of
	 *  minutes after current alarm.
	 */
	public final void snoozeAlarm() {
		time = Calendar.getInstance();
		Log.d("alarms", "sending snooze");
		Intent updatedAlarm = new Intent();
		updatedAlarm.setAction("snooze"); //send it to the alarmHandler
		updatedAlarm.putExtra(getString(R.string.time),
				time.getTimeInMillis() + SNOOZE_TIME_MILLIS);
		LocalBroadcastManager.getInstance(this).
		sendBroadcast(updatedAlarm);

		finishActivity();
	}
	/**
	 * Ends this activity.
	 */
	public final void finishActivity() {
		this.finish();
		}
	/**
	 * plays the sound of the alarm.
	 * @param context context of the calling activity
	 * @param alert The uri of the alarm sound
	 */
	private void playSound(final Context context, final Uri alert) {
	        try {
	        	Log.d("CONTEXT", "" + context);
	        	Log.d("URI", "" + alert);
	        	mMediaPlayer = new MediaPlayer();
	            mMediaPlayer.setDataSource(context, alert);
	            final AudioManager audioManager = (AudioManager) context
	                    .getSystemService(Context.AUDIO_SERVICE);
	            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM)
	            		!= 0) {
	                mMediaPlayer.setAudioStreamType(AudioManager.
	                		STREAM_ALARM);
	                mMediaPlayer.setOnPreparedListener(this);
	                mMediaPlayer.prepareAsync();
	            }
	        } catch (IOException e) {
	            System.out.println("OOPS");
	        }
	}
	@Override
	public final void onPrepared(final MediaPlayer mp) {
		mp.start();
	}

}
