/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.alarm;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import edu.wakemeapp.R;
import edu.wakemeapp.WakeUpFragment;

/**
 * Adapter for the list of alarms in WakeUpFragment.
 * @author kristofferskjutar
 *
 */
public class AlarmArrayAdapter extends ArrayAdapter<Long> implements OnClickListener, OnCheckedChangeListener  {
	  private final Context context;
	  private final List<Long> values;
	  private WakeUpFragment controller;
	  /**
	   * Consructs the object.
	   * @param context calling activity
	   * @param values times to set
	   * @param controller controlling class
	   */
	  public AlarmArrayAdapter(final Context context,final ArrayList<Long> values,final WakeUpFragment controller) {
	    super(context, R.layout.alarm_array_layout, values);
	    this.context = context;
	    this.values = values;
	    this.controller = controller;

	  }
	  /**
	   * Called for every added item.
	   * Sets up the image, text, button, switch and their listeners
	   */
	  @Override
	  public View getView(final int position,final View convertView,final ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(
	    		R.layout.alarm_array_layout, parent, false);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    TextView firstline = (TextView) rowView.
	    		findViewById(R.id.firstLiner);
	    TextView secondline = (TextView) rowView.
	    		findViewById(R.id.secondLiner);

	    Switch onOffAlarm = (Switch) rowView.findViewById(R.id.onOffAlarm);
	    onOffAlarm.setChecked(controller.
	    		getStateList().get(values.get(position)));
	    Button editButton = (Button) rowView.findViewById(R.id.editButton);

	    editButton.setOnClickListener(controller);
	    onOffAlarm.setOnCheckedChangeListener(controller);
	    editButton.setTag(values.get(position));
	    onOffAlarm.setTag(values.get(position));

	    SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    String time = date.format(new Date(values.get(position)));


 
	    String[] parts = time.split(" ");
	    firstline.setText(parts[1]);
	    secondline.setText(parts[0]);

	    imageView.setImageResource(R.drawable.wakemeapp_icon);


	    return rowView;
	  }
	  @Override
	  public int getCount() {
		   return values.size();
		}


	@Override
	public void onClick(final View v) {
		Toast.makeText(context, "" + v.getTag(),
				Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onCheckedChanged(final CompoundButton c, final boolean onOff) {
		Toast.makeText(context, "" + onOff, Toast.LENGTH_LONG).show();
	}
}
