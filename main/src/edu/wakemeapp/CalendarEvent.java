/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj���lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author kristofferskjutar och Mikael Stolpe
 *
 */
public class CalendarEvent implements Parcelable {
	/**
	 * Separator used when parsing and unparsing a calendar event.
	 */
	private static final String UNIQUE_SEPERATOR = "::::";
	/**
	 * Index constants used when rebuilding a CalendarEvent from a string.
	 */
	private static final int LONGITUDE_INDEX = 0;
	private static final int LATITUDE_INDEX = 1;
	private static final int EVENT_START_INDEX = 2;
	private static final int EVENT_END_INDEX = 3;
	private static final int EVENT_LOCATION_INDEX = 4;
	private static final int EVENT_ID_INDEX = 5;
	private static final int EVENT_NAME_INDEX = 6;
	private static final int CALENDAR_ID_INDEX = 7;
	private static final int CALENDAR_NAME_INDEX = 8;


	private double longitude;
	private double latitude;
	private long eventStart;
	private long eventEnd;
	private String eventLocation;
	private String eventName;
	private String eventID;
	private Long calendarId;
	private String calendarName;



	public static final Parcelable.Creator<CalendarEvent> CREATOR =
			new Parcelable.Creator<CalendarEvent>() {
		public CalendarEvent createFromParcel(final Parcel in) {
			return new CalendarEvent(in);
		}
		public CalendarEvent[] newArray(final int size) {
			return new CalendarEvent[size];
		}
	};

	/**
	 * Constructor for creating a CalendarEvent.
	 * @param longitude value for the longitude
	 * @param latitude value for the latitude
	 * @param eventStart start time for event from epoch
	 * @param eventEnd end time for event from epoch
	 * @param eventid id for the event
	 */
	public CalendarEvent(final double longitude, final double latitude, final long eventStart, final long eventEnd, final String eventid) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.eventID = eventid;
	}

	/**
	 * Constructor for CalendarEvent without GPS coord.
	 * @param eventName Name of event
	 * @param eventStart start time for event from epoch
	 * @param eventEnd end time for event from epoch
	 * @param eventLocation Location as string
	 * @param eventId id for the event
	 */
	public CalendarEvent(final String eventName, final long eventStart,final long eventEnd,final String eventLocation, final String eventId) {
		this.eventName = eventName;
		this.eventStart = eventStart;
		this.eventEnd = eventEnd;
		this.eventLocation = eventLocation;
		this.eventID = eventId;
	}

	/**
	 * Default constructor for event.
	 */
	public CalendarEvent() {
		this.longitude = 0;
		this.latitude = 0;
		this.eventStart = 0;
		this.eventEnd = 0;
	}

	/**
	 * Constructs a CalendarEvent from a specially constructed string.
	 * @param parsedString String which contains a parsed Events
	 */
	public CalendarEvent(final String parsedString) {
		String[] parsedArray = parsedString.split(UNIQUE_SEPERATOR);
		this.longitude = Double.parseDouble(parsedArray[LONGITUDE_INDEX]);
		this.latitude = Double.parseDouble(parsedArray[LATITUDE_INDEX]);
		this.eventStart = Long.parseLong(parsedArray[EVENT_START_INDEX]);
		this.eventEnd = Long.parseLong(parsedArray[EVENT_END_INDEX]);
		this.eventLocation = parsedArray[EVENT_LOCATION_INDEX];
		this.eventName = parsedArray[EVENT_NAME_INDEX];
		this.eventID = parsedArray[EVENT_ID_INDEX];
		this.calendarId = Long.parseLong(parsedArray[CALENDAR_ID_INDEX]);
		this.calendarName = parsedArray[CALENDAR_NAME_INDEX];

	}

	/**
	 * Parses a CalendarEvent to a string.
	 * Not overriding toString since this is specially used for sending events
	 * @return The event as a parsed String
	 */
	public final String parseToString() {
		return "" + longitude + UNIQUE_SEPERATOR + latitude  + UNIQUE_SEPERATOR	+ eventStart  + UNIQUE_SEPERATOR + eventEnd + UNIQUE_SEPERATOR + eventLocation + UNIQUE_SEPERATOR + eventID + UNIQUE_SEPERATOR + eventName + UNIQUE_SEPERATOR + calendarId + UNIQUE_SEPERATOR + calendarName;
	}
	/**
	 * Constructor for reading from parcel.
	 * @param in Parcel for in-stream
	 */
	public CalendarEvent(final Parcel in) {
		readFromParcel(in);
	}

	/**
	 * Reads a parcel and sets all variables.
	 * @param in Parcel for building object
	 */
	private void readFromParcel(final Parcel in) {
		this.longitude = in.readDouble();
		this.latitude = in.readDouble();
		this.eventStart = in.readLong();
		this.eventEnd = in.readLong();
		this.eventLocation = in.readString();
		this.eventName = in.readString();
		this.eventID = in.readString();
		this.calendarId = in.readLong();
		this.calendarName = in.readString();
	}

	/**
	 * Writes info to a given Parcel.
	 * @param dest destinaton parcel
	 * @param flags flags if needed
	 */
	@Override
	public final void writeToParcel(final Parcel dest, final int flags) {
		dest.writeDouble(longitude);
		dest.writeDouble(latitude);
		dest.writeLong(eventStart);
		dest.writeLong(eventEnd);
		dest.writeString(eventLocation);
		dest.writeString(eventName);
		dest.writeString(eventID);
		dest.writeLong(calendarId);
		dest.writeString(calendarName);
	}

	public final double getLongitude() {
		return this.longitude;
	}

	public final double getLatitude() {
		return latitude;
	}

	public final long getEventStartTime() {
		return eventStart;
	}

	public final long getEventEndTime() {
		return eventEnd;
	}

	public final void setLongitude(final double l) {
		this.longitude = l;
	}

	public final void setLatitude(final double l) {
		this.latitude = l;
	}

	public final void setEventStartTime(final long l) { 
		this.eventStart = l;
	}

	public final void setEventEndTime(final long l) {
		this.eventEnd = l;
	}

	public final String getId() {
		return eventID;
	}

	/**
	 * @return the eventLocation
	 */
	public final String getEventLocation() {
		return eventLocation;
	}

	/**
	 * @param eventLocation the eventLocation to set
	 */
	public final void setEventLocation(final String eventLocation) {
		this.eventLocation = eventLocation;
	}

	/**
	 * @return the eventName
	 */
	public final String getEventName() {
		return eventName;
	}

	/**
	 * @param eventName the eventName to set
	 */
	public final void setEventName(final String eventName) {
		this.eventName = eventName;
	}

	@Override
	public final int describeContents() {
		return 0;
	}

	@Override
	public final boolean equals(final Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof CalendarEvent)) {
			return false;
		}
		CalendarEvent other = (CalendarEvent) obj;
		return this.eventID.equals(other.eventID);

	}

	/**
	 * Hashes the event.
	 * @return hashCode
	 */
	public final int hashCode() {
		return 31 * eventID.hashCode();
	}
	/**
	 * @return the eventID
	 */
	public final String getEventID() {
		return eventID;
	}
	/**
	 * @param eventID the eventID to set
	 */
	public final void setEventID(final String eventID) {
		this.eventID = eventID;
	}

	/**
	 * @return the calendarId
	 */
	public final Long getCalendarId() {
		return calendarId;
	}

	/**
	 * @param id the calendarId to set
	 */
	public final void setCalendarId(final Long id) {
		this.calendarId = id;
	}

	/**
	 * @return the calendarName
	 */
	public final String getCalendarName() {
		return calendarName;
	}

	/**
	 * @param calendarName the calendarName to set
	 */
	public final void setCalendarName(final String calendarName) {
		this.calendarName = calendarName;
	}
}
