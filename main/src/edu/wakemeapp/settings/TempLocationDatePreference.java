/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.settings;

import java.util.Calendar;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.DatePicker;
import edu.wakemeapp.R;

/**
 * Sets an temporary home position between two dates.
 * @author Gustav Dahl
 *
 */
public class TempLocationDatePreference extends DialogPreference {
	private int firstDay = 0, firstMonth = 0, firstYear = 0;
	private int secondDay = 0, secondMonth  = 0, secondYear = 0;
	private long currentTime;
	private DatePicker firstPicker, secondPicker;
	private Calendar time, resetTime;
	private Context mContext;
	/**
	 * Sets which layout to use and sets the current time.
	 * @param context
	 * @param attrs
	 */
	public TempLocationDatePreference(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		setDialogLayoutResource(R.layout.datepickerlayout);
		mContext = context;
		time = Calendar.getInstance();
		resetTime = Calendar.getInstance();
	}
	/**
	 * inflates the xml layout and sets the pickers
	 * minimum date to todays date.
	 */
	@Override
	protected void onBindDialogView(final View view) {
	//	time = Calendar.getInstance();
		firstPicker = (DatePicker) view.findViewById(R.id.firstPicker);
		secondPicker = (DatePicker) view.
				findViewById(R.id.secondPicker);
		  firstPicker.setMinDate(time.getTimeInMillis());
    	  secondPicker.setMinDate(time.getTimeInMillis());
	    super.onBindDialogView(view);
	}
	/**
	 * When the preference is closed and the OK button is pressed.
	 */
	@Override
    protected void onDialogClosed(final boolean positiveResult) {
      super.onDialogClosed(positiveResult);

      if (positiveResult) {
    	  firstDay = firstPicker.getDayOfMonth();
    	  firstMonth = firstPicker.getMonth();
    	  firstYear = firstPicker.getYear();

    	  secondDay = secondPicker.getDayOfMonth();
    	  secondMonth = secondPicker.getMonth();
    	  secondYear = secondPicker.getYear();

    	  time.set(firstYear, firstMonth, firstDay);
    	  long fromDate = time.getTimeInMillis();

    	  time.set(secondYear, secondMonth, secondDay);
    	  long toDate = time.getTimeInMillis();

    	  //Reset the calendar for future use of this setting
    	  currentTime = resetTime.getTimeInMillis();
    	  time.setTimeInMillis(currentTime);

    	if (toDate > fromDate) {
    		String dateInterval = String.valueOf(fromDate)
    				+ "-" + String.valueOf(toDate);
    		if (dateInterval != null && callChangeListener(dateInterval)) {
    			persistString(dateInterval);
    		}
    	}
     }
    }

}

