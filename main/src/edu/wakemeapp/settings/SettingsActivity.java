/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.settings;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import edu.wakemeapp.FragmentTabs;

/**
 * Apps settings activity.
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 *
 */
public class SettingsActivity extends Activity {
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("settingsActivity", "starting settingsActivity");
        ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
        SettingsFragment frag = new SettingsFragment();
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, frag)
                .commit();
    }
    @Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch(item.getItemId()) {
		case android.R.id.home:
			Intent home = new Intent(this,FragmentTabs.class);
			home.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(home);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}