/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;

/**
 * Represent a dialogPreference with a timePicker.
 * @author kristofferskjutar
 *
 */
public class TimePickerPreference extends DialogPreference {


	private int lastHour = 0;
	private int lastMinute = 0;
	private TimePicker picker = null;

	public static int getHour(final int time) {
		return time / 60;
	}

	public static int getMinute(int time) {
		return time % 60;
	}

	public TimePickerPreference(Context ctxt, AttributeSet attrs) {
		super(ctxt, attrs);
	}



	/**
	 * Initialize the timepicker.
	 */
	 @Override
	 protected View onCreateDialogView() {
		 picker = new TimePicker(getContext());
		 picker.setIs24HourView(true);
		 return picker;
	 }

	 /**
	  * Set up time.
	  */
	 @Override
	 protected void onBindDialogView(final View v) {
		 super.onBindDialogView(v);

		 picker.setCurrentHour(lastHour);
		 picker.setCurrentMinute(lastMinute);
	 }

	 /**
	  * Called when dialog is closing.
	  * If the time was changed, convert it and save it
	  */
	 @Override
	 protected void onDialogClosed(final boolean positiveResult) {
		 super.onDialogClosed(positiveResult);

		 if (positiveResult) {
			 lastHour = picker.getCurrentHour();
			 lastMinute = picker.getCurrentMinute();
			 Integer time = timeConverting(lastHour, lastMinute);

			 if (time instanceof Integer) {
				 Log.d("wrong", "time: " + time);
			 }
			 if (callChangeListener(time)) {
				 persistInt(time);
				 setDefaultValue((Integer)time);
			 }
		 }
	 }
	 /**
	  * Returns the default value.
	  */
	 @Override
	 protected Object onGetDefaultValue(final TypedArray a, final int index) {

		 return a.getInt(index, timeConverting(1,0));
	 }
	 /**
	  * If we have a stored value, load it, else load default.
	  */
	 @Override
	 protected void onSetInitialValue(final boolean restoreValue, final Object defaultValue) {

		 int time = timeConverting(12, 0);
		 if (restoreValue) {
			 if (defaultValue == null) {
				 time = getPersistedInt((Integer)time);
				 Log.d("debug", "the time set : " + time);
			 } else {
				 time = getPersistedInt((Integer)defaultValue);
			 }
		 } else {
			 time = (Integer)defaultValue;
		 }

		 lastHour = getHour(time);
		 lastMinute = getMinute(time);
	 }

	 /**
	  * Converts time to a more convenient format.
	  * @param hour hour to convert
	  * @param minute minute to convert
	  * @return converted as int
	  */
	 private final Integer timeConverting(Integer hour, Integer minute) {
		 return (hour * 60) + minute;
	 }


}