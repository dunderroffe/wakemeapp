/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.settings;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Dialog for yes and no settings.
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 *
 */
public class YesNoDialogPreference extends DialogPreference {

	public YesNoDialogPreference(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public YesNoDialogPreference(final Context context, final AttributeSet attrs, final int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onDialogClosed(final boolean positiveResult) {
		super.onDialogClosed(positiveResult);
		Log.d("SettingsActivity", "" + positiveResult);
		persistBoolean(positiveResult);
	}
}