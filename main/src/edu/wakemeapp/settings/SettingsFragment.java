/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter,
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.settings;

import java.util.HashSet;
import java.util.Set;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import edu.wakemeapp.CalendarEvent;
import edu.wakemeapp.GPSTracker;
import edu.wakemeapp.GeoResponse;
import edu.wakemeapp.Geocode;
import edu.wakemeapp.R;
import edu.wakemeapp.database.CalendarDataSource;

/**
 * Fragment linked with settings activity.
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 *
 */

public class SettingsFragment extends PreferenceFragment
implements OnSharedPreferenceChangeListener, GeoResponse {

	private static final String KEY_CALENDAR = "key for chosing calendar";
	private static final String KEY_DELETE_EVENT = "key for deleting an event";
	private static final String SET_BUFFER_TIME = "Set buffer time for alarm";
	private static final String SET_EARLIEST_TIME = "Set the earliest time to wake up";
	private static final String SET_LATEST_TIME = "Set the lastest time to wake up";
	private static final String BUFFER_TIME = "buffer time";
	private static final String LATEST_TIME = "latest time";
	private static final String EARLIEST_TIME = "earliest time";
	private static final String KEY_GPS_HOME = "Key for GPSHOME";
	private static final String KEY_CLEAR_DB = "key for clearing database from settings";
	private static final String ACTION_SETTINGS_UPDATED = "edu.wakemeapp.settingsUpdated";
	private static final String KEY_MEANS_OF_TRAVEL = "means of travel";
	private static final String KEY_CLEAR_TEMP_HOME = "key for removing temp location from database";
	private static final String KEY_PARSED_EVENT = "key for parsed events";
	private static final String TEMP_LOCATION_KEY = "tempLocationKey";
	private static final String TEMP_LOCATION_DATES = "tempLocationDates";
	private static final int MINUTES_PER_HOUR = 60;
	private static final String KEY_TEXT_HOME = "key for setting home pos using text";
	private static final int DEFAULT_HOURS = 5;
	private static final int DEFAULT_MINUTES = 23;
	private final String home = "home";
	private final String tempHome = "tempHome";
	private CalendarDataSource mDb;
	private MultiSelectListPreference mCalendarList;
	private SharedPreferences mSettings;
	private Context mContext;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		mContext = getActivity();
	}

	/**
	 * Initiates the list of users calendars to the current values, if no parsing has been done the setting will be disabled.
	 */
	private void initCalendarList() {
		mCalendarList = (MultiSelectListPreference) findPreference(getString(R.string.keyCalendarMultiChoice));
		String ids = mSettings.getString(getString(R.string.keyCalendarIds), null);
		String names = mSettings.getString(getString(R.string.keyCalendarNames), null);
		if (ids == null || names == null) {
			mCalendarList.setEnabled(false);
			mCalendarList.setTitle(getString(R.string.titleNoCalendar));
		} else {
			Log.d("SettingsFragment", "Setting ids and names: " + ids +  " : " + names);
			mCalendarList.setEnabled(true);
			mCalendarList.setTitle(getString(R.string.titleChooseCalendar));
			mCalendarList.setEntryValues(ids.split(","));
			mCalendarList.setEntries(names.split(","));
			Set<String> set =  mSettings.getStringSet(getString(
					R.string.keySetValues), null);
			if (set == null || set.isEmpty()) {
				set = new HashSet<String>();
			}
			Log.d("SettingsFragment", "All set values: " + set);
			mCalendarList.setValues(set);
		}

	}

	/**
	 * Initiates fields and variables in the fragment, initates the xml-file.
	 */
	private void initSettingsFragment() {
		PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences,
				false);
		mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity().
				getBaseContext());
		mSettings.registerOnSharedPreferenceChangeListener(this);
		mDb = CalendarDataSource.getInstance(getActivity(), false);
		initCalendarList();
		initDeleteEvent();
	}

	/**
	 * Initiates the option to delete events, if the database is empty
	 * this setting will disabled.
	 */
	private void initDeleteEvent() {
		ListPreference listP = (ListPreference) findPreference(KEY_DELETE_EVENT);
		mDb.open();
		Set<CalendarEvent> events = mDb.getAllEvents();
		if (events.size() == 0) {
			listP.setEnabled(false);
		} else {
			listP.setEnabled(true);
			CharSequence[] eNames = new CharSequence[events.size()];
			CharSequence[] eValues = new CharSequence[events.size()];
			int noNames = 0;
			for (CalendarEvent e : events) {
				eValues[noNames] = e.getId();
				eNames[noNames++] = e.getEventName();
				// change to e.getEventName when database handles names!!
			}
			listP.setEntries(eNames);
			listP.setEntryValues(eValues);
			listP.setValue("");
		}
	}



	/**
	 * Handles what happens when a setting has been changed.
	 * TODO implement individual filters for each setting change so this can be
	 *  handled more effeciently in EventsHandler
	 */
	public final void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String key) {
		Editor editor = sharedPreferences.edit();

		if (key.equals(KEY_CALENDAR)) {
			Set<String> s = sharedPreferences.getStringSet(key, null);
			Log.d("SettingsFragment", "Set: " + s);
			notifyEventHandler();
		} else if (key.equals(KEY_GPS_HOME)) {
			Log.d("SettingsFragment", "KeyGPSHome_start");
			//This if statement is needed or else the code will run twice
			if (mSettings.getBoolean(KEY_GPS_HOME, false)) {
				Log.d("SettingsFragment", "KeyGPSHome");
				editor.putBoolean(KEY_GPS_HOME, false);

				GPSTracker gps = new edu.wakemeapp.GPSTracker(mContext);
				// check if GPS enabled
				if (gps.canGetLocation()) {
					mDb.open();
					mDb.deleteEvent(home);
					mDb.addEvent(home, "homelocation", gps.getLatitude(),
							gps.getLongitude());
					Log.d("SettingsFragment", "Lat: " + mDb.getEvent(home).
							getLatitude() + "// Long: " + mDb.getEvent(home).
							getLongitude());
					mDb.close();
					initDeleteEvent();

				} else {
					// Can't get location
					// Ask user to enable GPS/network in settings
					gps.showSettingsAlert();
				}
				gps = null;   // Solution for random crash
			}
		} else if (key.equals(KEY_CLEAR_DB)) {
			//This if statement is needed or else the code will run twice
			if (mSettings.getBoolean(KEY_CLEAR_DB, false)) {
				Log.d("SettingsFragment", "Clearing DB");
				editor.putBoolean(KEY_CLEAR_DB, false);
				editor.putStringSet(KEY_PARSED_EVENT, null);
				mDb.open();
				mDb.clear();
				mDb.close();
				initDeleteEvent();
			}
		} else if (key.equals(SET_BUFFER_TIME)) {
			int bufferTimeSet = mSettings.getInt(SET_BUFFER_TIME, MINUTES_PER_HOUR);
			editor.putInt(BUFFER_TIME, bufferTimeSet);
			notifyEventHandler();
		} else if (key.equals(SET_EARLIEST_TIME)) {
			int earliestTimeSet = mSettings.getInt(SET_EARLIEST_TIME, DEFAULT_HOURS * MINUTES_PER_HOUR);
			editor.putInt(EARLIEST_TIME, earliestTimeSet);
			notifyEventHandler();
		} else if (key.equals(SET_LATEST_TIME)) {
			int latestTimeSet = mSettings.getInt(SET_LATEST_TIME, DEFAULT_MINUTES * MINUTES_PER_HOUR);
			editor.putInt(LATEST_TIME, latestTimeSet);
			notifyEventHandler();
		} else if (key.equals(TEMP_LOCATION_KEY)) {
			String tempLocationDates = mSettings.getString(TEMP_LOCATION_KEY,"");
			editor.putString(TEMP_LOCATION_DATES, tempLocationDates);

			notifyEventHandler();
			GPSTracker gps = new edu.wakemeapp.GPSTracker(mContext);

			if (gps.canGetLocation()) {
				mDb.open();
				mDb.deleteEvent(tempHome);
				mDb.addEvent(tempHome, "tempHomeLocation", gps.getLatitude(), gps.getLongitude());
				//Add the dates to the database
				mDb.close();
				Log.d("SettingsFragment", "Lat: " + gps.getLatitude() + "// Long: " + gps.getLongitude());
				initDeleteEvent();
			} else {
				// Can't get location
				// Ask user to enable GPS/network in settings
				gps.showSettingsAlert();
			}
		} else if (key.equals(KEY_CLEAR_TEMP_HOME)) {
			editor.remove(TEMP_LOCATION_DATES);
			mDb.open();
			mDb.deleteEvent(tempHome);
			mDb.close();

		} else if (key.equals(KEY_DELETE_EVENT)) {
			ListPreference listP = (ListPreference) findPreference(key);
			String choice = listP.getValue();
			Log.d("SettingsFragment", choice);
			mDb.open();
			mDb.deleteEvent(choice);
			mDb.close();
			initDeleteEvent();
		} else if (key.equals(KEY_MEANS_OF_TRAVEL)) {
			ListPreference listP = (ListPreference) findPreference(key);
			Log.d("SettingsFragment", listP.getValue());
		} else if (key.equals(KEY_TEXT_HOME)) {
			String textAddress = sharedPreferences.getString(key, null);
			editor.putString(key, "");
			Log.d("key text home", "Retrieved text from preference: " + textAddress);
			if (!(textAddress.trim().equals(""))) {
				Geocode task = new Geocode();
				task.execute(new String[] {textAddress});
				task.delegate = this;
				Log.d("key text home", "Finished delegate in SettingsFragment");
			}
		}
		editor.commit();
	}

	@Override
	public void processFinish(final double lat, final double lng, final String str, final boolean status) {
		Log.d("key text home", "Entered process finish");
		if (status) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

			alertDialog.setTitle(R.string.homeAddress);
			alertDialog.setMessage(getString(R.string.titleIsHomeAddress) + "\n" + str);

			alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int which) {
					mDb.open();
					mDb.deleteEvent(home);
					mDb.addEvent(home, "homelocation", lat, lng);
					mDb.close();
					initDeleteEvent();
					dialog.cancel();
				}
			});

			alertDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int which) {
					dialog.cancel();
				}
			});
			alertDialog.show();
		} else {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

			alertDialog.setTitle(R.string.incorrectInput);
			alertDialog.setMessage(getString(R.string.incorrectInputMessage));
			alertDialog.setNeutralButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {
				public void onClick(final DialogInterface dialog, final int id) {
					dialog.cancel();
				}
			});
			alertDialog.show();
		}
	}

	/**
	 * Notifies the EventHandler a setting has been changed.
	 */
	private void notifyEventHandler() {
		Intent sendEvents = new Intent(ACTION_SETTINGS_UPDATED);
		getActivity().sendBroadcast(sendEvents);
	}

	@Override
	public void onResume() {
		super.onResume();
		initSettingsFragment();
		initDeleteEvent();   // Needs to update after setHomePos
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/**
	 * Checks which booleans are true and takes appropriate action when leaving
	 * the settings fragment.
	 */
	@Override
	public void onPause() {
		super.onPause();
	}
}


