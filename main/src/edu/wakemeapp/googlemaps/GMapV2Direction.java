/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj���lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */
package edu.wakemeapp.googlemaps;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * This class parses a XML-doc containing the directions info.
 * Methods for retrieving; status, duration, distance and list of points
 * @author Martin Helmersson
 *
 */
public class GMapV2Direction {

	/**
	 * Empty contstructor.
	 */
	public GMapV2Direction() {
	}

	/**
	 * Checks the XML 'status' tag for valid result.
	 * returns true if it contains valid directions
	 * @param doc XML directions info
	 * @return boolean
	 */
	public final boolean getStatus(final Document doc) {
		NodeList nl1 = doc.getElementsByTagName("status");
		Log.d("Trafik",nl1.item(0).getTextContent());
		if (nl1.item(0).getTextContent().equals("ZERO_RESULTS")) {
			Log.d("Trafik","RESULT STATUS:" + nl1.item(0).equals("ZERO_RESULTS"));
			return false;
		} else {
			return true;
		}
	}
	/**
	 * Gets duration info form the directions doc XML.
	 * @param doc Directions doc
	 * @return String containing the specific time the travel take
	 */
	public final String getDurationText(final Document doc) {
		NodeList nl1 = doc.getElementsByTagName("duration");
        Node node1 = nl1.item(nl1.getLength() - 1);
        NodeList nl2 = node1.getChildNodes();
        Node node2 = nl2.item(getNodeIndex(nl2, "text"));
        Log.d("length", "" + nl1.getLength());
		return node2.getTextContent();
	}

	/**
	 * Gets the distance info from the directions doc XML.
	 * @param doc Directions doc
	 * @return String containing the specific distance
	 */
	public final String getDistanceText(final Document doc) {
		NodeList nl1 = doc.getElementsByTagName("distance");
        Node node1 = nl1.item(nl1.getLength() - 1);
        NodeList nl2 = node1.getChildNodes();
        Node node2 = nl2.item(getNodeIndex(nl2, "text"));
        Log.i("DistanceText", node2.getTextContent());
		return node2.getTextContent();
	}

	/**
	 * Gets the the start address from the directions doc XML.
	 * aka the home position and returns it as a formatted String
	 * @param doc directions doc
	 * @return String with formatted address
	 */
	public final String getStartAddress(final Document doc) {
		NodeList nl1 = doc.getElementsByTagName("start_address");
        Node node1 = nl1.item(0);
        Log.i("StartAddress", node1.getTextContent());
		return node1.getTextContent();
	}

	/**
	 * Gets the end address from the doc.
	 * @param doc Directions doc
	 * @return Formatted String with the end address
	 */
	public final String getEndAddress(final Document doc) {
		NodeList nl1 = doc.getElementsByTagName("end_address");
        Node node1 = nl1.item(0);
        Log.i("StartAddress", node1.getTextContent());
		return node1.getTextContent();
	}

	/**
	 * Parses the subsegment of the route and saves them in a ArrayList of lat longs.
	 * @param doc Directions doc
	 * @return Arraylist with the lat longs for the complete route
	 */
	public final ArrayList<LatLng> getDirection(final Document doc) {
		NodeList nl1, nl2, nl3;
        ArrayList<LatLng> listGeopoints = new ArrayList<LatLng>();
        nl1 = doc.getElementsByTagName("step");
        if (nl1.getLength() > 0) {
            for (int i = 0; i < nl1.getLength(); i++) {
                Node node1 = nl1.item(i);
                nl2 = node1.getChildNodes();

                Node locationNode = nl2.item(getNodeIndex(nl2, "start_location"));
                nl3 = locationNode.getChildNodes();
                Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
                double lat = Double.parseDouble(latNode.getTextContent());
                Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                double lng = Double.parseDouble(lngNode.getTextContent());
                listGeopoints.add(new LatLng(lat, lng));

                locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "points"));
                ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
                for (int j = 0; j < arr.size(); j++) {
                	listGeopoints.add(new LatLng(arr.get(j).latitude, arr.get(j).longitude));
                }

                locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "lat"));
                lat = Double.parseDouble(latNode.getTextContent());
                lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                lng = Double.parseDouble(lngNode.getTextContent());
                listGeopoints.add(new LatLng(lat, lng));
            }
        }
        return listGeopoints;
	}
	/**
	 * private method for finding the index of the specific node.
	 * @param nl list of nodes
	 * @param nodename name of node
	 * @return the index of the node
	 */
	private int getNodeIndex(final NodeList nl, final String nodename) {
		for (int i = 0; i < nl.getLength(); i++) {
			if (nl.item(i).getNodeName().equals(nodename)) {
				return i;
			}
		}
		return -1;
	}
	/**
	 * Private method for decoding the polyline generated by google.
	 * @author Jeffrey Sambells <http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java>
	 * @param encoded string
	 * @return Arraylist of lat longs
	 */
	private final ArrayList<LatLng> decodePoly(final String encoded) {
		ArrayList<LatLng> poly = new ArrayList<LatLng>();
		int index = 0, len = encoded.length();
		int lat = 0, lng = 0;
		while (index < len) {
			int b, shift = 0, result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat;
			shift = 0;
			result = 0;
			do {
				b = encoded.charAt(index++) - 63;
				result |= (b & 0x1f) << shift;
				shift += 5;
			} while (b >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng;
			LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
			poly.add(position);
		}
		return poly;
	}
}
