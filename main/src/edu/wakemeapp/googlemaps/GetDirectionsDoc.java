/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj���lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */
package edu.wakemeapp.googlemaps;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
/**
 * Takes a given URL and makes a request to googles directions API
 * then creates at doc of the directions info
 * @author Martin Helmersson
 *
 */
public class GetDirectionsDoc extends AsyncTask<String, Void, Document> {
	public DirectionsResponse delegate = null;
	public final static String MODE_DRIVING = "driving";
	public final static String MODE_WALKING = "walking";

	/**
	 * Called when doInBackgroud is finished, passing the doc to travelFragment.
	 * by the interface
	 * @param doc takes the doc from the do in background
	 */
	protected final void onPostExecute(final Document doc) {
	       delegate.processFinish(doc);
		}

	/**
	 * This method is called when the task is executed in travelFragment.
	 * fetches directions by httpClient to doc
	 * @param  params the specific URL for the request between to places
	 * @return doc the XML document containing the directions
	 */
	@Override
	protected Document doInBackground(final String... params) {
		LatLng start = new LatLng(Double.parseDouble(params[0]),
				Double.parseDouble(params[1]));
		LatLng end = new LatLng(Double.parseDouble(params[2]),
				Double.parseDouble(params[3]));
		String url = "http://maps.googleapis.com/maps/api/directions/xml?"
        		+ "origin=" + start.latitude + "," + start.longitude
        		+ "&destination=" + end.latitude + "," + end.longitude
        		+ "&sensor=false&units=metric&mode=" + params[4];
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(url);
            HttpResponse response = httpClient.execute(httpPost, localContext);
            InputStream in = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
            		.newDocumentBuilder();
            Document doc = builder.parse(in);
            return doc;
        } catch (Exception e) {
            Log.d("doc creation" , e.toString());
        }
		return null;
	}
}
