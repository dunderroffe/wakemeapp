/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj���lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp.googlemaps;


import org.w3c.dom.Document;

/**
 * Interface for delegating data when the asynktask is finished.
 * @author Martin Helmersson
 *
 */
public interface DirectionsResponse {
	void processFinish(Document doc);
}
