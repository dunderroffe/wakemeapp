/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */
package edu.wakemeapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import edu.wakemeapp.database.CalendarDataSource;


/**
 * Use this class to correct coordinates in events and add the new events
 * to the database.
 * @author Erik Gil Forsman
 *
 */
public class CorrectBadLocation implements GeoResponse {

	private CalendarEvent event;
	private CalendarDataSource mDb;
	private Context mContext;
	private final boolean test = false;	// testmode

	/**
	 * Constructor for CorrectBadLocation.
	 * @param event The event that needs correction
	 * @param context context of calling class
	 * @param newAddress The new address, used to replace the old coords
	 *
	 */
	public CorrectBadLocation(final CalendarEvent event,final Context context, final String newAddress) {
		Log.d("cbl", "Setting up CorrectBadLocation");
		Log.d("cbl", "" + context);
		this.event = event;
		mContext = context;
		mDb = CalendarDataSource.getInstance(context, test);

		putNewAddress(newAddress);

	}

	/**
	 * Uses Geocode class to get the coords of the newAddress
	 * Receives the coords in the method processFinish.
	 * @param newAddress The new address, used to replace the old coords
	 */
	private void putNewAddress(final String newAddress) {
		Geocode task = new Geocode();
		task.execute(new String[] {newAddress});
		task.delegate = this;
		Log.d("cbl", "Running putNewAddress");
	}



	/**
	 * This method is called when the task in putNewAddress is finished.
	 * @param lat The latitude of newAddress
	 * @param lng The longitude of newAddress
	 * @param str The complete address (With city, zip code etc.)
	 * @param status status if JSON is good
	 */
	@Override
	public final void processFinish(final double lat, final double lng, final String str, final boolean status) {
		Log.d("cbl", "Status: " + status);
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
		String earliestEventAsString = sharedPref.getString(mContext.getString(R.string.earliestEvent), null);
		CalendarEvent earliestEvent = null;
		Log.d("cbl", "" + earliestEventAsString);
		if (earliestEventAsString != null) {
			earliestEvent = new CalendarEvent(earliestEventAsString);
		}
		if (status) {
			Log.d("cbl", "Place was found! :" + str);
			Log.d("cbl", "Printing NEW lat (1): " + lat);
			Log.d("cbl","Printing NEW long (1): " + lng);
			event.setLatitude(lat);
			event.setLongitude(lng);
			mDb.open();
			mDb.deleteEvent(event.getEventID());
			mDb.addEvent(event);
			Log.d("cbl", "Printing NEW lat (2): " + mDb.getEvent(event.getEventID())
					.getLatitude());
			Log.d("cbl", "Printing NEW long (2): " + mDb.getEvent(event.getEventID())
					.getLongitude());
			if (earliestEvent != null) {
				Log.d("cbl", "Current event: " + event.getEventID() + "Earliest event: " + earliestEvent.getEventID());
				// this event is the earliest?
				if (event.getEventID().equals(earliestEvent.getEventID())) {
					event.setEventName(mContext.getString(R.string.destination));
					event.setEventID(mContext.getString(R.string.destination));
					mDb.addEvent(event);
				}

			}
			mDb.close();
			Log.d("cbl", "" + mContext);

		} else {
			Log.d("cbl", "That place was not found!");
		}

	}
}
