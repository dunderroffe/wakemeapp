/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */
package edu.wakemeapp;

/**
 * Handles response for GeoCode.
 * @author Mikael Stolpe <mikaelstolpe1@gmail.com>
 */
public interface GeoResponse {
	/**
	 * called when proccess is finished.
	 * @param lat latitude for coordinate.
	 * @param lng longitude for coordinate
	 * @param str parsed string
	 * @param status true if JSON parse ok
	 */
    void processFinish(double lat, double lng, String str, boolean status);
}
