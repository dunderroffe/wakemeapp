/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import edu.wakemeapp.database.CalendarDataSource;
import edu.wakemeapp.settings.SettingsFragment;

/**
 * Responsible for filtering out times not passing users settings and sending times forward in the application.
 * Checks settings and parsed events and makes appropriate choices in accordance to this.
 * @author Mikael Stolpe
 *
 */
public class EventsHandler extends BroadcastReceiver {

	private CalendarDataSource mDb;
	private SharedPreferences mSettings;
	private Context mContext;
	private Set<String> mIdsSettings;
	private List<CalendarEvent> mReceivedEvents;
	private int mLatestTime;
	private int mBufferTime;
	private int mEarliestTime;
	private String dateString;
	private static final int DEFAULT_LATEST_TIME = 720;
	private static final int DEFAULT_EARLIEST_TIME = 60;
	private static final int DEFAULT_BUFFER_TIME = 300;
	private static final int CONVERT_FROM_MILLI = 60000;
	private static final int HOUR_CONVERT = 60;

	/**
	 * Receives notifications and filters out times.
	 * Then it sends all times which will be set to the ({@link FragmentTabs}
	 * TODO Add more filters in ({@link SettingsFragment} and user modular system to make Class more effecient
	 * 	 */
	@Override
	public final void onReceive(final Context context, final Intent intent) {

		this.mContext = context;
		initReceiver();
		String action = intent.getAction();
		Log.d("EventsHandler", action);
		if (action.equals(context.getString(R.string.actionParsingDone))) {
			handleReceivedEvents();
		}
		// same as above but different filter, shouldn't check everything as it is right now. Could be improved with more filter
		// and only calling appropriate methods
		if (action.equals(context.getString(R.string.actionSettingsUpdated))) {
			handleReceivedEvents();
		}

		if (action.equals(context.getString(R.string.actionRequestDestination))) {
			CalendarEvent earliestEvent = null;
			for (CalendarEvent event : mReceivedEvents) {
				if (earliestEvent == null) {
					earliestEvent = event;
				} else {
					if (earliestEvent.getEventStartTime() > event.getEventStartTime()) {
						earliestEvent = event;
					}
				}
			}
			setNextEvent(earliestEvent);
			Intent destinationSet = new Intent();
			destinationSet.setAction(context.getString(R.string.actionDestinationSet));
			LocalBroadcastManager.getInstance(context).sendBroadcast(destinationSet);

		}
	}

	/**
	 * Initiates the receiver, initates fields and sets settings.
	 */
	private void initReceiver() {
		if (mDb == null) {
			mDb = CalendarDataSource.getInstance(mContext, false);
		}
		this.mSettings = PreferenceManager.getDefaultSharedPreferences(mContext);
		setParsedEvents();
		initSettings();
	}

	/**
	 * Gets all the current parsed events.
	 */
	private void setParsedEvents() {
		Set<String> parsedEvents = mSettings.getStringSet(mContext.getString(R.string.keyParsedEvents), null);
		Set<String> actualEvents = null;
		Long current = new Date().getTime();
		mReceivedEvents = new ArrayList<CalendarEvent>();
		if (parsedEvents != null) {
			actualEvents = new TreeSet<String>();
			for (String eventString : parsedEvents) {
				CalendarEvent event = new CalendarEvent(eventString);
				if (event.getEventStartTime() > current) {
					mReceivedEvents.add(event);
					actualEvents.add(event.parseToString());
				}
			}
		}

		Editor edit = mSettings.edit();
		edit.putStringSet(mContext.getString(R.string.keyParsedEvents), actualEvents);
		edit.commit();
	}

	/**
	 * Calls methods to initiate settings.
	 */
	private void initSettings() {

		setLatestTime();
		setEarliestTime();
		setBufferTime();
		setSelectedIds();
	}

	/**
	 * Gets updated latest time from settings.
	 */
	private void setLatestTime() {

		mLatestTime = mSettings.getInt(mContext.getString(R.string.latestTime), DEFAULT_LATEST_TIME);
		Log.d("latest", "" + mLatestTime);
	}

	/**
	 * Gets updated buffer time from settings.
	 */
	private void setBufferTime() {
		mBufferTime = mSettings.getInt(mContext.getString(R.string.bufferTime), DEFAULT_EARLIEST_TIME);
	}

	/**
	 * Gets update earliest time from settings.
	 */
	private void setEarliestTime() {
		mEarliestTime = mSettings.getInt(mContext.getString(R.string.earliestTime), DEFAULT_BUFFER_TIME);
	}

	/**
	 * Gets updated selected ids from settings.
	 */
	private void setSelectedIds() {
		mIdsSettings = mSettings.getStringSet(mContext.getString(R.string.keyCalendarMultiChoice), null);
		Log.d("EventsHandler", "Status of settingsIds" + mIdsSettings);
		if (mIdsSettings == null) {
			mIdsSettings = new HashSet<String>();
		}
	}

	/**
	 * Adds events to database and sends them forward in the application.
	 */
	private void handleReceivedEvents() {
		List<Long> times = new ArrayList<Long>();
		List<CalendarEvent> receivedEvents = filterEvents(mReceivedEvents);
		for (CalendarEvent event : receivedEvents) {
			if (checkValidity(event)) {
				Log.d("EventsHandler", "Passed ValidityCheck: " + event.getEventName());
				times.add(event.getEventStartTime());

			}
			SimpleDateFormat d = new SimpleDateFormat("dd/MM HH:mm");
			Log.d("EventsHandler", "Received Event: '" + event.getEventName() + "' with time: '" + d.format(new Date(event.getEventStartTime())) + "'");
			Log.d("EventsHandler", "Unique ID of Event: " + event.getEventID());
			Log.d("EventsHandler", "Calendar ID of Event: " + event.getCalendarId());
		}

		sendTimes(checkWithSettings(times));
	}

	/**
	 * Sets the next event to occur.
	 * @param earliestEvent the next event
	 */
	private void setNextEvent(final CalendarEvent earliestEvent) {
		mDb.open();
		if (mDb.containsEvent(earliestEvent)) {
			CalendarEvent tempEvent = mDb.getEvent(earliestEvent.getEventID());
			earliestEvent.setLatitude(tempEvent.getLatitude());
			earliestEvent.setLongitude(tempEvent.getLongitude());

			Editor edit = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
			edit.putString(mContext.getString(R.string.earliestEvent), earliestEvent.parseToString());
			edit.commit();
		}
		mDb.close();
	}


	/**
	 * Checks if an event is considered valid by comparing calendar id.
	 *  and checking its time against buffer time, latest time and earliest time which is set in settings
	 * @param event to check
	 * @return true if valid
	 */
	private boolean checkValidity(final CalendarEvent event) {

		return checkId(event) && checkEarliestTime(event);
	}


	/**
	 * Check a calendarEvent with earliestTime settings.
	 * @param event to check
	 * @return true if valid
	 */
	private boolean checkEarliestTime(final CalendarEvent event) {

		Long startTime = event.getEventStartTime();
		int  intTime =  timeFormat(startTime - CONVERT_FROM_MILLI * mBufferTime);
		if ((intTime) < mEarliestTime) {
			Log.d("earliest", "false");
			return false;
		}
		return true;
	}

	/**
	 * Takes an event and comparse its calendar id with users selected ids from settings.
	 * @param event event to check
	 * @return true if valid event, false ow
	 */
	private boolean checkId(final CalendarEvent event) {
		Log.d("EventsHandler", "checkId, Events calendar id: " + event.getCalendarId());
		Log.d("EventsHandler", "checkId, mIdsSettings: " + mIdsSettings);
		Log.d("EventsHandler", "checkid: " + mIdsSettings.contains(Long.toString(event.getCalendarId())));
		return mIdsSettings.contains(Long.toString(event.getCalendarId()));
	}


	/**
	 * Sends the times to (@link FragmentTabs).
	 * @param times list of times to send
	 */
	private void sendTimes(final List<Long> times) {
		// Prepares and sends times to the recevier which registers filter actionSendTimes
		long[] timesToSend = new long[times.size()];
		for (int i = 0; i < times.size(); i++) {
			timesToSend[i] = times.get(i);
			Log.d("EventsHandler", "Sending Times" + times.get(i));
		}
		if (!times.isEmpty()) {
			Intent sendTimes = new Intent();
			sendTimes.setAction(mContext.getString(R.string.actionSendTimes));
			sendTimes.putExtra(mContext.getString(R.string.keyParsedTimes), timesToSend);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(sendTimes);
		}
	}

	/**
	 * Checks if certain times fit time-based critera user has set in settings.
	 * @param unCheckedTimes List of times to check
	 * @return List of passed times
	 */
	private List<Long> checkWithSettings(final List<Long> unCheckedTimes) {
		ArrayList<Long> passedList = new ArrayList<Long>();

		for (Long time : unCheckedTimes) {

			time -= (CONVERT_FROM_MILLI * mBufferTime);
			SimpleDateFormat date = new SimpleDateFormat("HH:mm");
			String stringtime = date.format(new Date(time));
			String[] parts = stringtime.split(":");
			int hourTime = (Integer.parseInt(parts[0])) * HOUR_CONVERT;
			int minuteTime = Integer.parseInt(parts[1]) + hourTime;

			Log.d("EventsHandler", "latest: " + mLatestTime + " earliest: " + mEarliestTime);
			Log.d("EventsHandler", minuteTime + "");
			if (time > Calendar.getInstance().getTimeInMillis()) {
				if (minuteTime > mLatestTime) {

					Calendar c = Calendar.getInstance();
					c.setTimeInMillis(time);
					//if(minuteTime>actualTime) //if you already should be awake
					c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
							c.get(Calendar.DAY_OF_MONTH), (mLatestTime / HOUR_CONVERT), (mLatestTime % HOUR_CONVERT));
					passedList.add(c.getTimeInMillis()); //-60000*bufferTime);
				} else {
					passedList.add(time);
				}
			}

		}
		Log.d("EventsHandler", "Size of passed list: " + passedList.size());
		return passedList;

	}

	/**
	 * Converts to minute.
	 * @param longTime time as long from epoch
	 * @return returns a minute representation
	 */
	private int timeFormat(final Long longTime) {
		SimpleDateFormat date = new SimpleDateFormat("HH:mm");
		String stringtime = date.format(new Date(longTime));
		String[] parts = stringtime.split(":");
		int hourTime = (Integer.parseInt(parts[0])) * HOUR_CONVERT;
		int minuteTime = Integer.parseInt(parts[1]) + hourTime;
		return minuteTime;
	}


	/**
	 * Filters out all non-relevent events for the parser. Such as events after the first of the day
	 * @param events to filter
	 * @return a filtered array
	 */
	private List<CalendarEvent> filterEvents(final List<CalendarEvent> events) {
		HashMap<String, CalendarEvent> onlyFirst = new HashMap<String, CalendarEvent>();
		for (CalendarEvent e : events) {
			Log.d("EventsHandler", "Events before filtering first only: " + e.getEventName());
			SimpleDateFormat s = new SimpleDateFormat("dd");
			String day = s.format(new Date(e.getEventStartTime()));
			if (!onlyFirst.containsKey(day)) {
				onlyFirst.put(day, e);
			} else {
				if (onlyFirst.get(day).getEventStartTime() > e.getEventStartTime()) {
					onlyFirst.put(day, e);
				}
			}
		}

		for (CalendarEvent e: onlyFirst.values()) {
			Log.d("EventsHandler", "Event after filtering first only: " + e.getEventName());
			Log.d("EventsHandler", "Event with name: '" + e.getEventName() + "' belongs to calendar with id: '" + e.getCalendarId() + "'");
		}
		return new ArrayList<CalendarEvent>(onlyFirst.values());
	}
}


