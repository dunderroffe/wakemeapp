/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter,
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Asks the user to verify location of a CalendarEvent
 * @author Viktor Sj�lind
 *
 */
public class Verifier extends DialogFragment {
	private static final int MAX_VALUE = 20;
	public static final String CALENDAR_EVENT = "CalendarEvent";

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    LayoutInflater inflater = getActivity().getLayoutInflater();
	    View mView = inflater.inflate(R.layout.verifier_layout, null);
	    Bundle b = getArguments();
	    if (b != null) {
	    	final CalendarEvent event = b.getParcelable(CALENDAR_EVENT);

	    	final TextView eventName = (TextView) mView.findViewById(R.id.text);
	    	eventName.setText(event.getEventName());

	    	final EditText location = (EditText) mView.findViewById(R.id.edit);
		    location.setText(event.getEventLocation());

	    	builder.setTitle(R.string.verifier_title);
		    builder.setView(mView)

	           .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
	               @Override
	               public void onClick(final DialogInterface dialog, final int id) {
	                   new CorrectBadLocation(event, getActivity(), location.getText().toString());
	                   Log.d("Verifier", "Location Corrected: ");
	               }
	           });
	    }
	    return builder.create();
	}
}