/*
 * WakeMeApp is an Android application that helps organize the users morning.
 * Copyright (C) 2013  Erik Gil Forsman, Gustav Dahl, Kevin Vetter, 
 * Martin Helmersson, Viktor Sj�lind, Kristoffer Skjutar, Mikael Stolpe.
 * 
 * The full notice is found in wakemeapp/main/license.txt
 */

package edu.wakemeapp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.Toast;
import edu.wakemeapp.database.CalendarDataSource;
/**
 * Gets all the calendars associated with users account and puts the names and ids of these Calendars in SharedPreferences so these
 * can be used in the application
 * It also gets all the Events from all the Calendars and saves these so they can be used in the application
 * TODO Possible todo is to created a database to save these events instead.
 * How to use Fragment: create bundle for fragment in activity which creates this fragment.
 *
 * @author Mikael Stolpe
 *
 */
public class CalendarParseFragment extends Fragment {
	/**
	 * Constant array which specify which columns to get from the calendar.
	 */
	public static final String[] EVENT_COLUMNS = new String[]{
		CalendarContract.Events.TITLE,
		CalendarContract.Instances.BEGIN,
		CalendarContract.Instances.END,
		CalendarContract.Events.EVENT_LOCATION,
		CalendarContract.Instances.EVENT_ID,
		CalendarContract.Instances.CALENDAR_ID,
		CalendarContract.Instances.CALENDAR_DISPLAY_NAME,
		CalendarContract.Instances.CALENDAR_TIME_ZONE
	};

	/**
	 * Index constants for the EVENT_COLUMS array.
	 */
	private static final int COLUMNS_TITLE_INDEX = 0;
	private static final int COLUMNS_DTSTART_INDEX = 1;
	private static final int COLUMNS_DTEND_INDEX = 2;
	private static final int COLUMNS_LOCATION_INDEX = 3;
	private static final int COLUMNS_EVENT_ID_INDEX = 4;
	private static final int COLUMNS_CALENDAR_ID_INDEX = 5;
	private static final int COLUMNS_CALENDAR_NAME_INDEX = 6;
	private static final int COLUMNS_CALENDAR_TIME_ZONE_INDEX = 7;


	private static final int SELECTION_ARGS_NR = 1;

	private String[] mSelectionArgs;
	private ContentResolver mCResolver;
	private int mDays;
	private int mHours;

	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRetainInstance(true);
		initFragment();
		DownloadEventsTask dTask = new DownloadEventsTask();
		dTask.execute();
	}
	/**
	 * Initiates the fragments fields.
	 */
	private void initFragment() {
		getBundleParams();
		mCResolver = getActivity().getContentResolver();
	}
	/**
	 * Simply sets all the params recieved in the bundle. These needs to be set before activating the intent
	 * If this isn't successfully completed it informs the user what went wrong and cancels the fragment.
	 * Bundle should be packed with "account name" (keyAccountName), "account type" (keyAccountType)
	 * "owner name" (keyOwnerName) and "days" (keyDaysAhead). The last specifies how many days one
	 * wishes to parse ahead.
	 */
	private void getBundleParams() {
		mSelectionArgs = new String[SELECTION_ARGS_NR];
		Bundle b;
		if (getArguments() == null) {
			b = new Bundle();
		} else {
			b = getArguments();
		}
		String accountName = b.getString(getString(R.string.keyAccountName));
		String accountType = b.getString(getString(R.string.keyAccountType));
		String ownerName = b.getString(getString(R.string.keyOwnerName));
		if (accountName == null) {
			Toast.makeText(getActivity(), getString(R.string.errorAccountName), Toast.LENGTH_LONG).show();
			getActivity().getFragmentManager().popBackStack();
		} else if (accountType == null) {
			Toast.makeText(getActivity(), getString(R.string.errorAccountType), Toast.LENGTH_LONG).show();
			getActivity().getFragmentManager().popBackStack();
		} else if (ownerName == null) {
			Toast.makeText(getActivity(), getString(R.string.errorOwnerName), Toast.LENGTH_LONG).show();
			getActivity().getFragmentManager().popBackStack();
		}
		mSelectionArgs[0] = accountName;

		mDays = b.getInt(getString(R.string.keyDaysAhead));
		if (mDays == 0) {
			Toast.makeText(getActivity(), getString(R.string.errorTimeNotSet), Toast.LENGTH_LONG).show();
			mDays = 1;

		}
		mHours = 1; // hard-coded since this fits our current needs, might stay this way for relase.
		Log.d("CalendarParseFragment", "Days ahead set to: " + mDays);


	}


	/**
	 * Gets all the Events associated with the calendarIds and returns a list with these events.
	 * Puts the Ids of the calendars into each CalendarEvent
	 * @return All the events associated to the account
	 */
	private List<CalendarEvent> getCalendarEvents() {
		HashSet<CalendarEvent> calendarEvents =  new HashSet<CalendarEvent>();
		CalendarEvent foundEvent = null;
		Map<Long, String> mapNameId = new HashMap<Long, String>();
		Long current = new Date().getTime();
		SimpleDateFormat s = new SimpleDateFormat("dd/HH:mm");
		String day = s.format(current);
		Log.d("CalendarParseFragment", "Current Time: " + day);
		Long future = current + (DateUtils.DAY_IN_MILLIS * mDays) + (DateUtils.HOUR_IN_MILLIS * mHours);

		Cursor cur = CalendarContract.Instances.query(mCResolver, EVENT_COLUMNS, current, future);
		if (cur.getCount() > 0) {
			while (cur.moveToNext()) {
				foundEvent = createEvent(cur);
				Log.d("CalendarParseFragment", "CalendarContract.Instances.Title from event: '" + cur.getString(COLUMNS_TITLE_INDEX) + "'");
				Log.d("CalendarParseFragment", "CalendarContract.Instances.CALENDAR_ID from event: '" + cur.getLong(COLUMNS_CALENDAR_ID_INDEX) + "'");
				Log.d("CalendarParseFragment", "CalendarContract.Instances.CALENDAR_NAME from event: '" + cur.getString(COLUMNS_CALENDAR_NAME_INDEX) + "'");
				Log.d("CalendarParseFragment", "CalendarContract.Instances.CALENDAR_TIMEZONE from event: '" + cur.getString(COLUMNS_CALENDAR_TIME_ZONE_INDEX) + "'");
				if (foundEvent.getEventStartTime() > current) {
					calendarEvents.add(foundEvent);
					mapNameId.put(cur.getLong(COLUMNS_CALENDAR_ID_INDEX), cur.getString(COLUMNS_CALENDAR_NAME_INDEX));
				}
			}
		}
		updateSettings(mapNameId);
		return new ArrayList<CalendarEvent>(calendarEvents);
	}

	/**
	 * Updates the calendar settings parameters, to ensure no unnecessary times are sent.
	 * @param mapNameId Map with names of calendars and its id to update settings with
	 */
	private void updateSettings(final Map<Long, String> mapNameId) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		Set<String> chosenCalendars = pref.getStringSet(getString(R.string.keyCalendarMultiChoice), null);
		String alreadyParsed = pref.getString(getString(R.string.keyCalendarIds), null);
		List<String> alreadyParsedList = new ArrayList<String>();
		if (alreadyParsed != null) {
			alreadyParsedList = Arrays.asList(alreadyParsed.split(","));
		}

		Editor edit = pref.edit();
		StringBuffer ids = new StringBuffer();
		StringBuffer names = new StringBuffer();
		for (Long id : mapNameId.keySet()) {
			ids.append(id);
			ids.append(",");
			if (chosenCalendars != null  && !alreadyParsedList.contains(id.toString())) {
				chosenCalendars.add(id.toString());
			}

		}
		if (ids.length() > 0) {
			ids.deleteCharAt(ids.length() - 1);
		}
		for (String s : mapNameId.values()) {
			names.append(s);
			names.append(",");
		}
		if (names.length() > 0) {
			names.deleteCharAt(names.length() - 1);
		}
		Log.d("CalendarParseFragment", "Commiting Ids: '" + ids.toString() + "' to shared preferences");
		Log.d("CalendarParseFragment", "Commiting names: '" + names.toString() + "' to shared preferences");
		edit.putString(getString(R.string.keyCalendarIds), ids.toString());
		edit.putString(getString(R.string.keyCalendarNames), names.toString());
		edit.putStringSet(getString(R.string.keySetValues), chosenCalendars);
		edit.commit();
	}

	/**
	 * Creates an CalendarEvent from the cursor info.
	 * @param cur cursor to parse
	 * @return CalendarEvent
	 */
	private CalendarEvent createEvent(final Cursor cur) {
		CalendarEvent event = new CalendarEvent(cur.getString(COLUMNS_TITLE_INDEX), cur.getLong(COLUMNS_DTSTART_INDEX),
				cur.getLong(COLUMNS_DTEND_INDEX), cur.getString(COLUMNS_LOCATION_INDEX), cur.getString(COLUMNS_EVENT_ID_INDEX));
		event.setCalendarId(cur.getLong(COLUMNS_CALENDAR_ID_INDEX));
		event.setCalendarName(cur.getString(COLUMNS_CALENDAR_NAME_INDEX));
		return event;

	}

	/**
	 * Handles all the parsed Events, saves the events so they can be accessed in the EventsHandler.
	 * Then notifies the EventsHandler to update the alarms
	 * @param events All events to send
	 */
	private void handleParsedEvents(final List<CalendarEvent> events) {
		saveParsedEvents(events);
		CalendarEvent[] parsedEvents = new CalendarEvent[events.size()];
		Log.d("parsedEvent", events.toString());
		events.toArray(parsedEvents);
		CalendarDataSource db = CalendarDataSource.getInstance(getActivity(), false);
		db.open();
		for (CalendarEvent e : parsedEvents) {
			if (!db.containsEvent(e)) {
				System.err.println(e.getEventName());
				Verifier v = new Verifier();

				Bundle bundle = new Bundle();
				bundle.putParcelable(Verifier.CALENDAR_EVENT, e);
				v.setArguments(bundle);
				v.show(getFragmentManager(), "test");
				while (v.isVisible()) {
				}

			}
		}
		db.close();
		Intent sendEvents = new Intent(getString(R.string.actionParsingDone));
		getActivity().sendBroadcast(sendEvents);
		getActivity().getFragmentManager().popBackStack();
	}

	/**
	 * Saves the parsed event to the shared preferences of the application so they can be accessed in EventsHandler.
	 * @param events Events to save
	 */
	private void saveParsedEvents(final List<CalendarEvent> events) {
		Set<String> savedEvents =  new HashSet<String>();
		for (CalendarEvent event : events) {
			savedEvents.add(event.parseToString());
		}
		Editor edit = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
		edit.putStringSet(getString(R.string.keyParsedEvents), savedEvents);
		edit.commit();
	}

	/**
	 * Downloads all the Events related to the account.
	 * @author Mikael Stolpe
	 *
	 */
	private class DownloadEventsTask extends AsyncTask<String, Integer, List<CalendarEvent>> {

		@Override
		protected List<CalendarEvent> doInBackground(final String... params) {
			return getCalendarEvents();
		}

		@Override
		protected void onProgressUpdate(final Integer... e) {
			Toast.makeText(getActivity(), getString(R.string.errorParsingCalendar), Toast.LENGTH_LONG).show();
			getActivity().getFragmentManager().popBackStack();
		}

		@Override
		protected void onPostExecute(final List<CalendarEvent> events) {
			if (events.isEmpty()) {
				publishProgress();
			}
			handleParsedEvents(events);
		}


	}

}
