package edu.wakemeapp.ui;

import java.util.Calendar;

import com.android.uiautomator.core.*;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;
/**
 * This TestCase performs blackbox testing on the alarms by checking triggering
 * and manipulating
 * @author kristofferskjutar
 *
 */
public class AlarmUiTest extends UiAutomatorTestCase{

	private static final String loadCalendar = "Load Calendar";
	private static final String edit = "Edit";
	private static final String cancel = "Cancel";

	public void setUp() throws UiObjectNotFoundException 
	{

	}

	public void test() throws UiObjectNotFoundException
	{
		clickButton(loadCalendar);
		getUiDevice().waitForIdle();
		assertTrue(testAlarm(1)); //in 1 minute
		
		clickButton(loadCalendar);
		getUiDevice().waitForIdle();
		assertTrue(testAlarm(-1)); //in the past
		
		clickButton(loadCalendar);
		alarmToggle();
		assertFalse(testAlarm(1)); //alarm off
		
		clickButton(loadCalendar);
		alarmToggle();
		alarmToggle();
		assertTrue(testAlarm(-1)); //alarm off/on
		
	}
	
	
	/**
	 * toggles the first alarms on/off switch
	 * @throws UiObjectNotFoundException
	 */
	private void alarmToggle() throws UiObjectNotFoundException
	{
		UiObject frame = new UiObject(new UiSelector().className(android.widget.FrameLayout.class).index(1));
		UiObject parent = frame.getChild(new UiSelector().className(android.widget.RelativeLayout.class).index(0));
		UiObject button = parent.getChild(new UiSelector().className(android.widget.Switch.class));
		button.click();
	}
	
	/**
	 * Clicks on a button with the given text
	 * @param text
	 * @throws UiObjectNotFoundException
	 */
	private void clickButton(String text) throws UiObjectNotFoundException
	{
		UiObject uiObject = new UiObject(new UiSelector().text(text));
		uiObject.click();
	}
	
	
	/**
	 * adds the amount of minutes to an already displayed TimePicker
	 * @param amount
	 * @throws UiObjectNotFoundException
	 */
	private void addMinuteToPicker(int amount) throws UiObjectNotFoundException
	{
		UiObject scroll = new UiObject(new UiSelector().scrollable(true).index(2));
		UiObject child = scroll.getChild(new UiSelector().index(1));
		String oldTime = child.getText();

		Integer oldInt = Integer.parseInt(oldTime);
		
		oldInt=oldInt+amount;
		String newTime = ""+oldInt;
		child.setText(newTime);
		//child.click();
	}


	/**
	 * Sets the alarm to a given time (minute) from now and returns if the alarm triggered 
	 * @param i , given minute
	 * @return boolean if the alarm triggered
	 * @throws UiObjectNotFoundException
	 */
	private boolean testAlarm(int i) throws UiObjectNotFoundException {	
		
		clickButton(edit);

		addMinuteToPicker(i);

		UiObject set = new UiObject(new UiSelector().className(android.widget.Button.class).clickable(true).index(1));
		set.clickAndWaitForNewWindow();
		Calendar c = Calendar.getInstance();
		int minute = c.get(Calendar.MINUTE);
		String s = cancel;
		UiObject cancel = new UiObject(new UiSelector().text(s));
		while(true)
		{
			if(cancel.exists())
				break;
			if(Calendar.getInstance().get(Calendar.MINUTE)==(minute+1))
				return false; //to long time (today???)
		}

		
		return cancel.click();
	}
	
	
	

}
