package edu.wakemeapp.ui;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.core.UiWatcher;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

public class TestWakeMeFragment extends UiAutomatorTestCase {
	 
	private UiObject travelTab = new UiObject(new UiSelector()
    .text("travel"));
	
	private UiObject wakeUpTab = new UiObject(new UiSelector()
    .text("wakeUpTab"));
	
	private UiObject parseButton = new UiObject(new UiSelector()
	.text("LOAD CALENDAR"));
	
	
	
	public void testWakeMeFragment() throws UiObjectNotFoundException
	{
		parseFromStart();
		assertTrue(wakeUpTab.click());
		checkNewAlarm();
		travelTab.click();
		wakeUpTab.click();
		checkNewAlarm(); //saved alarms?
	
	}
	
	private void checkNewAlarm()
	{
		getUiDevice().pressDPadDown();
		getUiDevice().pressDPadDown();
		getUiDevice().pressDPadDown();
		assertTrue(getUiDevice().pressDPadDown()); //successfully loaded at least 1 new alarm
	}
	
	private void parseFromStart() throws UiObjectNotFoundException
	{
		assertTrue(travelTab.click());
//		assertTrue(parseButton.click());
	}
	
}
