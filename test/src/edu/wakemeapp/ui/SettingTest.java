package edu.wakemeapp.ui;


import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

public class SettingTest extends UiAutomatorTestCase {
	
	private static final long MAX_WAIT = 1000;
	private static final String CANCEL = "Cancel";
	private static final String OK = "OK";
	private static final String NO = "No";
	private static final String YES = "Yes";
	
	private static final String EARLIEST_TIME_TO_WAKE_UP = "Set the earliest time to wake up";
	private static final String LATEST_TIME_TO_WAKE_UP = "Set the latest time to wakeup";
	private static final String CHOOSE_CALENDARS = "Choose calendars";
	private static final String MEENS_OF_TRAVEL = "Set preferred means of travel";
	private static final String CLEAR_DATABASE = "Clear Database";
	private static final String BUFFER_TIME = "Set buffer time for alarm";
	private static final String HOME_LOCATION = "Set home location";
	private static final String TEMP_HOME_LOCATION = "Set temporary home location";
	private static final String GROUP_SET_TIMES = "Set Times";
	private static final String GROUP_SET_LOCATIONS = "Set Locations";
	private static final String GROUP_OTHER = "Other";
	
	public void setUp() throws UiObjectNotFoundException {
		enterSettings();
		
	}
	
	public void tearDown() {
		pressBack(); // exit settings menu
	}
	
	
	public void testEnterClearDatabaseNo() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(CLEAR_DATABASE);
		press(NO);
		pressBack(); // exit sub menu
	}

	public void testEnterClearDatabaseYes() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(CLEAR_DATABASE);
		press(YES);
		pressBack(); // exit sub menu
	}
	
	public void testEnterClearDatabaseBack() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(CLEAR_DATABASE);
		pressBack();
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetMeensOfTravelOK() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(MEENS_OF_TRAVEL);
		press(OK);
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetMeensOfTravelCancel() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(MEENS_OF_TRAVEL);
		press(CANCEL);
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetMeensOfTravelBack() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(MEENS_OF_TRAVEL);
		pressBack();
		pressBack(); // exit sub menu
	}
	
	public void testEnterChooseCalendarsOK() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(CHOOSE_CALENDARS);
		press(OK);
		pressBack(); // exit sub menu
	}

	public void testEnterChooseCalendarsCancel() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(CHOOSE_CALENDARS);
		press(CANCEL);
		pressBack(); // exit sub menu
	}
	
	public void testEnterChooseCalendarsBack() throws Exception {
		enterSetting(GROUP_OTHER);
		enterSetting(CHOOSE_CALENDARS);
		pressBack();
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetEarliestTimeToWakeUpOK() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(EARLIEST_TIME_TO_WAKE_UP);
		press(OK);
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetEarliestTimeToWakeUpCancel() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(EARLIEST_TIME_TO_WAKE_UP);
		press(CANCEL);
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetEarliestTimeToWakeUpBack() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(EARLIEST_TIME_TO_WAKE_UP);
		pressBack();
		pressBack(); // exit sub menu
	}

	public void testEnterSetLatestTimeToWakeUpOK() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(LATEST_TIME_TO_WAKE_UP);
		press(OK);
		pressBack(); // exit sub menu
	}
	
	public void testEnterSetLatestTimeToWakeUpCancel() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(LATEST_TIME_TO_WAKE_UP);
		press(CANCEL);
		pressBack(); // exit sub menu
	}

	public void testEnterSetLatestTimeToWakeUpBack() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(LATEST_TIME_TO_WAKE_UP);
		pressBack();
		pressBack(); // exit sub menu
	}


	public void testEnterBufferTimeOK() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(BUFFER_TIME);
		press(OK);
		pressBack(); // exit sub menu
	}
	
	public void testEnterBufferTimeCancel() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(BUFFER_TIME);
		press(CANCEL);
		pressBack(); // exit sub menu
	}

	public void testEnterBufferTimeBack() throws Exception {
		enterSetting(GROUP_SET_TIMES);
		enterSetting(BUFFER_TIME);
		pressBack();
		pressBack(); // exit sub menu
	}
	public void testEnterHomeLocationNo() throws Exception {
		enterSetting(GROUP_SET_LOCATIONS);
		enterSetting(HOME_LOCATION);
		press(NO);
		pressBack(); // exit sub menu
	}

	public void testEnterHomeLocationYes() throws Exception {
		enterSetting(GROUP_SET_LOCATIONS);
		enterSetting(HOME_LOCATION);
		press(YES);
		pressBack(); // exit sub menu
	}
	
	public void testEnterHomeLocationBack() throws Exception {
		enterSetting(GROUP_SET_LOCATIONS);
		enterSetting(HOME_LOCATION);
		pressBack();
		pressBack(); // exit sub menu
	}
		
	public void testEnterTempHomeLocationOK() throws Exception {
		enterSetting(GROUP_SET_LOCATIONS);
		enterSetting(TEMP_HOME_LOCATION);
		press(OK);
		pressBack(); // exit sub menu
	}

	public void testEnterTempHomeLocationCancel() throws Exception {
		enterSetting(GROUP_SET_LOCATIONS);
		enterSetting(TEMP_HOME_LOCATION);
		press(CANCEL);
		pressBack(); // exit sub menu
	}
	
	public void testEnterTempHomeLocationBack() throws Exception {
		enterSetting(GROUP_SET_LOCATIONS);
		enterSetting(TEMP_HOME_LOCATION);
		pressBack();
		pressBack(); // exit sub menu
	}
	
	private void enterSetting(String name) throws UiObjectNotFoundException {
		getUiDevice().waitForIdle();
		
		UiObject meansOfTravel = pickNext(name);
		while (!meansOfTravel.exists()) {
			assertTrue(getUiDevice().pressDPadDown());
			meansOfTravel = pickNext(name);
		}
		
		assertTrue(meansOfTravel.clickAndWaitForNewWindow());
	}

	private UiObject pickNext(String name) {
		return new UiObject(new UiSelector().selected(true).text(name));
	}

	private void press(String text) throws Exception{
		getUiDevice().waitForIdle();
		
		UiObject btn  = new UiObject(new UiSelector().text(text));
		btn.click();
		
		assertTrue(btn.waitUntilGone(MAX_WAIT));
	}
	

	private void enterSettings() throws UiObjectNotFoundException {
		getUiDevice().waitForIdle();
		assertTrue(getUiDevice().pressMenu());
		assertTrue(getUiDevice().pressDPadUp());
		assertTrue(getUiDevice().pressEnter());		
	}
	
	private void pressBack() {
		getUiDevice().pressBack();
		getUiDevice().waitForIdle();
	}
	
	
}
