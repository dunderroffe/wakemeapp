package edu.wakemeapp.databaseTest;
/*
 * Authors Erik Forsman & Viktor Sj�lind
 */
import java.util.NoSuchElementException;
import java.util.Set;

import android.test.AndroidTestCase;
import android.util.Log;
import edu.wakemeapp.CalendarEvent;
import edu.wakemeapp.database.CalendarDataSource;


public class DatabaseSourceTest extends AndroidTestCase{

	private CalendarDataSource source;
	private static final String TEST_EVENT_ID = "idTest";
	private static final String TEST_EVENT_NAME = "eventName";
	private static final String BAD_EVENT_ID = "BAD_EVENT_ID";
	private static final double LONGITUDE = 12.14;
	private static final double LATITUDE = 33.55;
	private static final boolean TEST = true;
	
	public void setUp() throws Exception {
		super.setUp();
		Log.d("database", "Setting up test  enivironment");
		source = CalendarDataSource.getInstance(getContext(), TEST);
		source.open();
	}
	
	public void testAdd() {
		source.addEvent(TEST_EVENT_ID, TEST_EVENT_NAME, LATITUDE, LONGITUDE);
		assertTrue(source.containsEvent(TEST_EVENT_ID));
	}
	
	public void print() {
		Set<CalendarEvent> set = source.getAllEvents();
		for (CalendarEvent x : set) {
			Log.d("database", x.getId());
		}
		Log.d("database","klar");
	}
	
	public void testGetEvent() {
		source.addEvent(TEST_EVENT_ID, TEST_EVENT_NAME, LATITUDE, LONGITUDE);
		assertEquals(TEST_EVENT_ID, source.getEvent(TEST_EVENT_ID).getId());
		print();
	}
	
	public void testClear() {
		for(int x = 0; x<10; x++) {
			source.addEvent(TEST_EVENT_ID+x, TEST_EVENT_NAME, LATITUDE, LONGITUDE);
		}
		source.clear();
		assertTrue(source.getAllEvents().isEmpty());
	}
	
	public void testContains() {
		source.addEvent(TEST_EVENT_ID, TEST_EVENT_NAME, LATITUDE, LONGITUDE);
		CalendarEvent calE = new CalendarEvent(LONGITUDE, LATITUDE, 0, 0, TEST_EVENT_ID);
		assertTrue(source.containsEvent(calE));
		assertTrue(source.containsEvent(TEST_EVENT_ID));
	}
	
	public void testGetAllEvents() {
		source.addEvent(TEST_EVENT_ID, TEST_EVENT_NAME, LATITUDE, LONGITUDE);
		source.addEvent(TEST_EVENT_ID+10, TEST_EVENT_NAME, LATITUDE, LONGITUDE);
		Set<CalendarEvent> set = source.getAllEvents();
		
		assertTrue(set.size() == 2);
		 
		for (CalendarEvent x : source.getAllEvents()) {
			assertTrue(source.containsEvent(x));
		} 
		
	}
	
	public void testSetNameNullWillFail() {
		source.addEvent(TEST_EVENT_ID, null, LATITUDE, LONGITUDE);
		
		try {
			source.getEvent(BAD_EVENT_ID);			
			assertTrue(false);
		} catch(NoSuchElementException e) {
			assertTrue(true);
		}
	}
	
	public void testGetNoneExistingID() {
		try {
			source.getEvent(BAD_EVENT_ID);
			assertTrue(false);
		} catch(NoSuchElementException e) {
			assertTrue(true);
		}
	}
	
	
	public void testGetNoneExistingIDNUll() {
		try {
			source.getEvent(null);
			assertTrue(false);
		} catch(IllegalArgumentException e) {
			assertTrue(true);
		}
	}
	
	public void tearDown(){
		Log.d("database", "Tearing down test  enivironment");
		source.clear();
		source.close();
	}

}
