The buildAndRun script requires that ANDROID_HOME is set as
an enivronment variable, pointing to the android_sdk folder.

The tests that are run are hardcoded into the script file.

To use the buildAndRun.sh script simply type:

sh buildAndRun.sh <path to test wakemeapp> <name of project>

In this case we will always have test as name of project.

Example:

sh buildAndRun.sh ~/workspace/WakeMeApp/ test
