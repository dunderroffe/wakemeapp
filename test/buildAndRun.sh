#!/bin/bash

if [ -z "$ANDROID_HOME" ]; then
	echo "ANDORID_HOME is not set!"
else

# Build project and push to device
$ANDROID_HOME/tools/android create uitest-project -n test -t 1 -p $1/$2
ant build
$ANDROID_HOME/platform-tools/adb push bin/$2.jar /data/local/tmp

# Run tests
$ANDROID_HOME/platform-tools/adb shell uiautomator runtest $2.jar -c edu.wakemeapp.ui.AlarmUiTest
$ANDROID_HOME/platform-tools/adb shell uiautomator runtest $2.jar -c edu.wakemeapp.ui.SettingTest
$ANDROID_HOME/platform-tools/adb shell uiautomator runtest $2.jar -c edu.wakemeapp.ui.TestWakeMeFragment

fi
